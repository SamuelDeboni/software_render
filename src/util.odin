package main

Fixed_List :: struct(T: typeid) {
	data: []T,
	len: int,
}

fl_slice :: proc(list: Fixed_List($T)) -> []T {
	return list.data[0:list.len]
}

fl_clear :: proc(list: ^Fixed_List($T)) {
	for _, idx in list.data do list.data[idx] = {}
	list.len = 0
}

fl_push :: proc(list: ^Fixed_List($T), val: T) -> (error: bool) {
	if list.len >= len(list.data) do return true
	list.data[list.len] = val
	list.len += 1
	return
}

fl_pop :: proc(list: ^Fixed_List($T)) -> (result: T) {
	if list.len > 0 {
		result = list.data[list.len - 1]
		list.len -= 1
	}
	return
}

Static_List :: struct(T: typeid, size: int) {
	data: [size]T,
	len: int,
}

sl_slice :: #force_inline proc(list: ^Static_List($T, $S)) -> []T {
	return list.data[:int(list.len)]
}

sl_clear :: proc(list: ^Static_List($T, $S)) {
	for i: int = 0; i < len(list.data); i += 1 do list.data[i] = {}
	list.len = 0
}

sl_insert :: proc(list: ^Static_List($T, $S), value: T, pos: int) {
	if int(list.len) >= len(list.data) do return

	list.len += 1
	for i := list.len; i > pos; i -= 1 do list.data[i] = list.data[i - 1]
	list.data[pos] = value
}

sl_remove :: proc(list: ^Static_List($T, $S), pos: int) -> (value: T) {
	if list.len == 0 do return

	value = list.data[pos]

	list.len -= 1
	for i := pos; i < list.len; i += 1 do list.data[i] = list.data[i + 1]

	return
}

sl_push :: proc(list: ^Static_List($T, $S), val: T) -> (error: bool) {
	if list.len >= len(list.data) do return true
	list.data[list.len] = val
	list.len += 1
	return
}

sl_pop :: proc(list: ^Static_List($T, $S)) -> (result: T) {
	if list.len > 0 {
		result = list.data[list.len - 1]
		list.len -= 1
	}
	return
}

// Value needs to be comparable
sl_search :: proc(list: Static_List($T, $S), value: $V, cmp: proc(a: T, b : V) -> bool) -> (result: int) {
	list := list
	result = -1
	for v, idx in sl_slice(&list) {
		if cmp(v, value) {
			result = idx
			break
		}
	}
	return
}
