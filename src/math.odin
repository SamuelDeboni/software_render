package main

import la "core:math/linalg"
import "core:math"

Vec2 :: [2]f32

tsin :: proc {
	tsin_f32,
	tsin_f64,
}

tsin_f32 :: #force_inline proc(v: f32) -> f32 {
	return math.sin(v * math.TAU)
}

tsin_f64 :: #force_inline proc(v: f64) -> f64 {
	return math.sin(v * math.TAU)
}

tcos :: proc {
	tcos_f32,
	tcos_f64,
}

tcos_f32 :: #force_inline proc(v: f32) -> f32 {
	return math.cos(v * math.TAU)
}

tcos_f64 :: #force_inline proc(v: f64) -> f64 {
	return math.cos(v * math.TAU)
}

hsv_to_rgb :: proc(hsv: [3]f32) -> (rgb: [3]f32) {
	h := hsv[0]
	s := hsv[1]
	v := hsv[2]

	c := s * v
	x := c * (1.0 - abs(math.mod(h / 60.0, 2) - 1.0))
	m := v - c

	if h < 60 {
		rgb = {c, x, 0}
	} else if h < 120 {
		rgb = {x, c, 0}
	} else if h < 180 {
		rgb = {0, c, x}
	} else if h < 240 {
		rgb = {0, x, c}
	} else if h < 300 {
		rgb = {x, 0, c}
	} else {
		rgb = {c, 0, x}
	}

	rgb = rgb + m
	return rgb
}

rgb_to_hsv :: proc(c: [3]f32) -> (hsv: [3]f32) {
	max := max(c.r, max(c.g, c.b))
	min := min(c.r, min(c.g, c.b))

	hsv[2] = max

	if hsv[2] > 0 {
		hsv[1] = (max - min) / max
	} else {
		hsv[1] = 0
	}

	if max == c.r {
		hsv[0] = (c.g - c.b) / (max - min)
	} else if max == c.g {
		hsv[0] = 2 + (c.b - c.r) / (max - min)
	} else {
		hsv[0] = 4 + (c.r - c.g) / (max - min)
	}
	hsv[0] *= 60
	hsv[0] = clamp(hsv[0], 0, 360)

	return
}
