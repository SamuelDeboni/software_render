package ui

import "core:fmt"

Percentage     :: distinct f32
Dynamic_W_Type :: distinct f32
Fill_W_Type    :: distinct f32

DYNAMIC_W : Dynamic_W_Type : 0
FILL_W    : Fill_W_Type    : 0

P :: #force_inline proc(v: f32) -> Percentage {
	return cast(Percentage)v
}

PATTERN :: proc(times_to_repeat: int, values: ..Width) -> (widths: []Width) {
	widths = make([]Width, times_to_repeat * len(values), context.temp_allocator)

	p := 0
	for i := 0; i < times_to_repeat; i += 1 {
		for v in values {
			widths[p] = v
			p += 1
		}
	}

	return
}

PATTERN_COMBO :: proc(_widths: []Width, times_to_repeat: int, values: ..Width) -> (widths: []Width) {
	widths = make([]Width, len(_widths) + times_to_repeat * len(values), context.temp_allocator)

	p := 0

	for w in _widths {
		widths[p] = w
		p += 1
	}

	for i := 0; i < times_to_repeat; i += 1 {
		for v in values {
			widths[p] = v
			p += 1
		}
	}

	return
}

Width :: union {int, f32, Percentage, Dynamic_W_Type, Fill_W_Type}

CENTER                      : u8 : 0x00
LEFT                        : u8 : 0x01
RIGHT                       : u8 : 0x02
SPACE_BEETWEEN              : u8 : 0x03
SPACE_BEETWEEN_WITH_BORDERS : u8 : 0x04
ALIGNMENT_MASK              : u8 : 0x0f

LOOP                        : u8 : 0x00
NO_LOOP                     : u8 : 0x10
LOOP_MASK                   : u8 : 0xf0

Alignment :: enum {
	Center,
	Left,
	Right,
	SpaceBetween,
	SpaceBetweenWithBorders,
}

Unit :: enum {
	Percentage,
	Px,
}

Layout_Unit :: struct {
	active:               bool,
	rect:                 Rect,
	father:               ^Layout_Unit,
	children:             ^Layout_Unit,
    next:                 ^Layout_Unit,
	loop_in_next:         bool,
	fixed_height:         bool,
	fixed_height_inner_h: f32,
	dynamic_width:        bool,
}

start_overlay_layout :: proc() {
	c.prev_layouts[c.prev_layouts_qnt] = c.layout_p 
	c.prev_layouts_qnt += 1

	c.layout_p = new_layout_unit()
	c.layout_p.rect = Rect{ w=c.frame.width, h=c.frame.height }
	c.layout_p.fixed_height = true

	split_row(0, P(1.0))
	fix_a_size_column_by_remaining(0)
}

end_overlay_layout :: proc() {
	c.prev_layouts_qnt -= 1
	c.layout_p = c.prev_layouts[c.prev_layouts_qnt]
}

fix_a_size_column :: proc(height: f32) {
	c.layout_p.fixed_height = true
	c.layout_p.fixed_height_inner_h = c.layout_p.rect.h
	c.layout_p.rect.h = height
}

reduce_size_column :: proc(decrement: f32) {
	c.layout_p.rect.h -= decrement
}

fix_a_size_column_by_remaining :: proc(height: f32) {
	c.layout_p.fixed_height = true
	c.layout_p.fixed_height_inner_h = c.layout_p.rect.h
	c.layout_p.rect.h = c.layout_p.father.rect.h - (c.layout_p.rect.y - c.layout_p.father.rect.y) - height
}

reserve_line :: proc(height: f32, width: f32 = 0) -> (rect: Rect) {
	if c.layout_p.dynamic_width && width > c.layout_p.rect.w {
		c.layout_p.rect.w = width
	}

	rect.x = c.layout_p.rect.x
	rect.y = c.layout_p.rect.y + (c.layout_p.fixed_height ? c.layout_p.fixed_height_inner_h : c.layout_p.rect.h)
	rect.w = c.layout_p.rect.w
	rect.h = height

	if !c.layout_p.fixed_height {
		c.layout_p.rect.h += height
	} else {
		c.layout_p.fixed_height_inner_h += height
	}

	return
}

space :: proc(height: f32) {
	if !c.layout_p.fixed_height {
		c.layout_p.rect.h += height
	} else {
		c.layout_p.fixed_height_inner_h += height
	}
}

pop_layout :: proc() -> (old_layout_p: ^Layout_Unit) {
	c.layout_p = c.layout_p.father

	highest_height: f32 = 0

	for it := c.layout_p.children; it != nil; it = it.next {
		h := (it.rect.y - c.layout_p.rect.y) + it.rect.h
		if h > highest_height {
			highest_height = h
		}

		it.active = false
		if it.loop_in_next do break
	}

	if !c.layout_p.fixed_height {
		c.layout_p.rect.h = highest_height
	} else {
		c.layout_p.fixed_height_inner_h = highest_height
	}

	if c.layout_p.dynamic_width {
		w: f32 = 0
		for it := c.layout_p.children; it != nil; it = it.next {
			w += it.rect.w
			if it.loop_in_next do break
		}
		c.layout_p.rect.w = w
	}

	old_layout_p = c.layout_p.children
	c.layout_p.children = nil
	return
}

new_layout_unit :: proc() -> ^Layout_Unit {
	for i := 0; i < MAX_LAYOUT_UNITS; i += 1 {
		if !c.layout_units[i].active {
			unit := &c.layout_units[i]
			unit^ = Layout_Unit{}
			unit.active = true
			return unit
		}
	}
	panic("Full Layout Units!")
}

split_row :: proc(flags: u8, columns_widths: ..Width) {
	x    := c.layout_p.rect.x
	step: f32 = 0
	empty_space := c.layout_p.rect.w

	fill_cols_qnt := 0
	for it in columns_widths {
		#partial switch v in it {
		case Fill_W_Type:
			fill_cols_qnt += 1
		}
	}

	if !c.layout_p.dynamic_width {
		for it in columns_widths {
			#partial switch v in it {
			case int:
				empty_space -= f32(it.(int))
			case f32:
				empty_space -= it.(f32)
			case Percentage:
				empty_space -= c.layout_p.rect.w * f32(it.(Percentage))
			}
		}

		if fill_cols_qnt <= 0 {
			switch flags & ALIGNMENT_MASK {
			case CENTER:
				x += empty_space / 2
				step = 0
			case LEFT:
				step = 0
			case RIGHT:
				x += empty_space
				step = 0
			case SPACE_BEETWEEN:
				step = empty_space / (f32(len(columns_widths)) - 1)
			case SPACE_BEETWEEN_WITH_BORDERS:
				step = empty_space / (f32(len(columns_widths)) + 1)
				x += step
			}
		}
	}

	assert(len(columns_widths) > 0)

//	p.children = new_layout_unit()
//	unit_p := p.children

	unit_p: ^Layout_Unit
	first := true
	for it in columns_widths {
		if !first {
			unit_p.next = new_layout_unit()
			unit_p = unit_p.next
		} else {
			c.layout_p.children = new_layout_unit()
			unit_p = c.layout_p.children
			first = false
		}		

		unit_p.father = c.layout_p
		unit_p.rect.x = x
		unit_p.rect.y = c.layout_p.rect.y + 
			(c.layout_p.fixed_height ? c.layout_p.fixed_height_inner_h : c.layout_p.rect.h)

		switch v in it {
		case Dynamic_W_Type:
			unit_p.rect.w = 0
			unit_p.dynamic_width = true
		case Fill_W_Type:
			unit_p.rect.w = empty_space / f32(fill_cols_qnt)
		case int:
			unit_p.rect.w = f32(it.(int))
		case f32:
			unit_p.rect.w = it.(f32)
		case Percentage:
			unit_p.rect.w = c.layout_p.rect.w * f32(it.(Percentage))
		}
		x += unit_p.rect.w + step
	}

	if (flags & LOOP_MASK) == LOOP {
		unit_p.loop_in_next = true
		unit_p.next = c.layout_p.children
	}

	c.layout_p = c.layout_p.children
}

float_column :: proc(rect: Rect, dynamic_width := false) {
	c.layout_p.children = new_layout_unit()
	c.layout_p.children.father = c.layout_p
	c.layout_p.children.rect = Rect{
		x = c.layout_p.rect.x + rect.x,
		y = c.layout_p.rect.y + rect.y,
		w = rect.w,
		h = rect.h,
	}
	c.layout_p.children.dynamic_width = dynamic_width

	c.layout_p = c.layout_p.children
}

next_column :: proc() {
	if c.layout_p.next != nil {
		if !c.layout_p.loop_in_next {
			if c.layout_p.dynamic_width {
				for it := c.layout_p.next; it != nil; it = it.next {
					it.rect.x += c.layout_p.rect.w
					if it.loop_in_next do break
				}
			}

			c.layout_p = c.layout_p.next
		} else {
			c.layout_p = c.layout_p.next

			highest_height: f32
			for it := c.layout_p; true; it = it.next {
				if it.rect.h > highest_height {
					highest_height = it.rect.h
				}

				if it.loop_in_next do break
			}

			for it := c.layout_p; true; it = it.next {
				it.rect = Rect{
					x = it.rect.x,
					y = it.rect.y + highest_height,
					w = it.rect.w,
					h = 0,
				}
				it.fixed_height = false
				it.fixed_height_inner_h = 0

				if it.loop_in_next do break
			}
		}
	} else {
		pop_layout()
	}
}

/*
is_above_vertical_limits :: proc(offset: f32 = 0) -> (is_above: bool, how_far: f32) {
	next_y := c.layout_p.rect.y + (c.layout_p.fixed_height ? c.layout_p.fixed_height_inner_h : c.layout_p.rect.h) + offset
	next_y += father_brick().state.scroll_offset.y 
	
	for it := c.layout_p; ; it = it.father {
		if it.fixed_height {
			return next_y < it.rect.y, (it.rect.y - next_y)
		}
	}

	panic("This is strange")
}*/

is_below_vertical_limits :: proc(offset: f32 = 0) -> (is_below: bool, how_far_beyond: f32) {
	next_y := c.layout_p.rect.y + (c.layout_p.fixed_height ? c.layout_p.fixed_height_inner_h : c.layout_p.rect.h) + offset
	next_y += father_brick().state.scroll_offset.y 

	for it := c.layout_p; ; it = it.father {
		if it.fixed_height {
			last_y := it.rect.y + it.rect.h
			return next_y > last_y, (next_y - last_y)
		}
	}

	panic("This is strange")
}

how_far_to_vertical_bottom :: proc() -> (how_far: f32)  {
	next_y := c.layout_p.rect.y + (c.layout_p.fixed_height ? c.layout_p.fixed_height_inner_h : c.layout_p.rect.h)
	next_y += father_brick().state.scroll_offset.y 

	for it := c.layout_p; ; it = it.father {
		if it.fixed_height {
			last_y := it.rect.y + it.rect.h
			return last_y - next_y
		}
	}

	panic("This is strange")
}

how_far_to_vertical_top :: proc() -> (how_far: f32)  {
	next_y := c.layout_p.rect.y + (c.layout_p.fixed_height ? c.layout_p.fixed_height_inner_h : c.layout_p.rect.h)
	next_y += father_brick().state.scroll_offset.y 

	for it := c.layout_p; ; it = it.father {
		if it.fixed_height {
			return next_y - it.rect.y
		}
	}

	panic("This is strange")
}

/* get_vertical_limits :: proc() -> (top_bounds: f32, end_bounds: f32) {
	for it := c.layout_p; ; it = it.father {
		if it.fixed_height {
			last_y := it.rect.y + it.rect.h
			return it.rect.y, last_y
		}
	}

	panic("This is strange")
} */

/*
is_below_vertical_limits :: proc(brick: ^Brick) -> (is_below: bool, how_far: f32) {
	brick_last_y := brick.rect.y + brick.rect.h
	brick_last_y += father_brick().state.scroll_offset.y 

	for it := c.layout_p; ; it = it.father {
		if it.fixed_height {
			last_y := it.rect.y + it.rect.h
			return brick_last_y > last_y, (brick_last_y - last_y)
		}
	}

	panic("This is strange")
} */

last_fixed_layout :: proc() -> ^Layout_Unit {
	for it := c.layout_p; it != nil; it = it.father {
		if it.fixed_height {
			return it
		}
	}

	panic("This is strange")
}