package ui

import "core:fmt"
import "core:runtime"
import "core:math"
import "core:hash"
import "core:strings"
import utf8 "core:unicode/utf8"

Editing_Buffer_Ctx :: struct {
	cursor:           int,
	virtual_size:     int,
	line_offset:      int,
	commit_line:      bool,
	virtual_v_offset: f32,
	str:              string,
}

init_buffer_and_editing_ctx :: proc(buffer: []rune, buf_ctx: ^Editing_Buffer_Ctx, text: string) {
	buf_ctx^ = Editing_Buffer_Ctx{}
	buf_ctx.str = text

	for r, i in utf8.string_to_runes(text, context.temp_allocator) {
		buffer[i] = r
		buf_ctx.virtual_size += 1
	}
}

Widget_Provided_Data :: struct {
	idle:                bool,
	hovering:            bool,
	clicked:             bool,
	down_clicked:        bool,
	holding:             bool,
	checked:             bool,
	changed:             bool,
	focused:             bool,
	start_focus:         bool,
	left_dragging:       bool,
	dragging_vector:     [2]f32,
	mouse_event_rel_pos: [2]f32,
	value_int:           int,
	str:                 string,
	error:               ^bool,
	error_msg:           ^string,
}

WData :: Widget_Provided_Data

Brick_Mouse_State :: enum {
	Idle,
	Hovering,
	Clicked,
	Holding,
	Fake_Holding,
}

Brick_State_Flags :: enum {
	Checked,
	Moving,
}

Brick_State :: struct {
	mouse:          Brick_Mouse_State,
	checked:        b8,
	scroll_offset:  [2]f32,
	cached_pos:     [2]f32,
	res_size:       [2]f32,
	glow:           f32,
	focus_glow:     f32,
	inv_opacity:    f32,
	cursor:         int,
	cursor_active_uptime: f32,
	value_int:      int,
	color:          [4]f32,
	error:          bool,
	error_msg:      string,
	leading_zeroes: i8,
}

Id_Gen_Mode :: enum {
	FatherLoc,
	FatherLocIndex,
	FatherLocChilduuid,
	Fixed,
}

Id_Gen_Blueprint :: struct {
	mode:  Id_Gen_Mode,
	index: int,
	fixed: u64, 
}

DEFID :: Id_Gen_Blueprint{}

IDXID :: #force_inline proc(index: int) -> Id_Gen_Blueprint {
	return Id_Gen_Blueprint{ mode=.FatherLocIndex, index=index }
}

INCID :: Id_Gen_Blueprint{ mode=.FatherLocChilduuid }

Text_Align :: enum {
	Middle_Center,

	Top_Left,
	Top_Center,
	Top_Right,
	Middle_Left,
	Middle_Right,
	Bottom_Left,
	Bottom_Center,
	Bottom_Right,
}

Primitive_Kind :: enum {
	Text,
	Cursor,
	Resource,
	Rect_Button,
	Rect_Input,
	Rect_Dialog,
	Rect_Box,
	Rect_Icon,
	Rect_Marker_Unchecked,
	Rect_Marker_Checked,
	Rect_Tabbed_Left,
	Rect_Tabbed_Top,

	Shape_Filled_Square,
	Shape_Outline_Square,
}

Primitive_Cursor :: struct {
	pos:     [2]f32,
	size:    f32,
	opacity: f32,
}

Text_Flags :: u8
TEXT_MUTED       : Text_Flags : 0b0000_0001
TEXT_BOLD        : Text_Flags : 0b0000_0010
TEXT_ITALIC      : Text_Flags : 0b0000_0100
TEXT_HIGHLIGHTED : Text_Flags : 0b0000_1000

Text_Size :: enum {
	Normal,
	Small,
	Big,
}

Text_Move_Direction :: enum {
	None,
	Left,
	Right,
	Up,
	Down,
}

Primitive_Text :: struct {
	pos:     [2]f32,
	content: string,
	flags:   Text_Flags,
	size:    f32,
}

Primitive_Rect :: struct {
	bounds:      Rect,
	glow:        f32,
	focus_glow:  f32,
	mouse_state: Brick_Mouse_State,
}

Primitive_Shape :: struct {
	bounds: Rect,
	radius: f32,
	color:  [4]f32,
}

Primitive_Resource :: struct {
	bounds: Rect,
	id:     i32,
}

Primitive :: struct {
	kind: Primitive_Kind,
	using u: struct #raw_union {
		text:     Primitive_Text,
		rect:     Primitive_Rect,
		shape:    Primitive_Shape,
		resource: Primitive_Resource,
		cursor:   Primitive_Cursor,
	},
}

Text_Draw_Blueprint :: struct {
	s:        string,
	align:    Text_Align,
	flags:    Text_Flags,
	size:     Text_Size,
	padding:  [4]f32,
	move_dir: Text_Move_Direction,
}

Brick :: struct {
	rect:                  Rect,
	text:                  string,
	id:                    u32,
	widget_kind:           Widget_Kind,
	widget_sub_kind:       int,
	state:                 Brick_State,
	time_on_state:         f32,
	father:                ^Brick,
	next:                  ^Brick,
	children:              ^Brick,
	child_next_aux_uuid:   int,
	resource_handle:       i32,
	resource_size:         [2]f32,
	temporary_int:         int,
	is_old:                bool,
}

default_update_glow_callback :: proc(glow: ^f32, mouse_state: Brick_Mouse_State) {
	switch mouse_state {
	case .Idle:
		glow^ = max(glow^ - c.frame.delta * 16, 0)
	case .Hovering:
		glow^ = min(glow^ + c.frame.delta * 8, 1)
	case .Holding:
	case .Fake_Holding:
	case .Clicked:
	}
}

default_update_focus_glow_callback :: proc(focus_glow: ^f32, focused: bool) {
	if !focused {
		focus_glow^ = max(focus_glow^ - c.frame.delta * 8, 0)
	} else {
		focus_glow^ = 1
	}
}

default_get_cursor_opacity_callback :: proc(cursor_active_uptime: f32) -> f32 {
	return (math.sin(cursor_active_uptime * 5) + 1) / 2
}

default_get_translation_by_glow :: proc(glow: f32, direction: Text_Move_Direction) -> [2]f32 {
	return {math.sin(glow * (math.PI / 2)) * 10, 0}
}

push_blueprint_texts :: proc(bounds: Rect, blueprints: []Text_Draw_Blueprint, text_glow: f32 = 0) {
	for it in blueprints {
		it_bounds := Rect {
			x = bounds.x + it.padding[0],
			y = bounds.y + it.padding[1],
			w = bounds.w - it.padding[0] - it.padding[2],
			h = bounds.h - it.padding[1] - it.padding[3],
		}

		if it.move_dir != .None {
			offset := c.get_translation_by_glow(text_glow, it.move_dir)
			it_bounds.x += offset.x
			it_bounds.y += offset.y
		}

		it_font_size: f32
		switch it.size {
		case .Normal:
			it_font_size = c.sizes.normal_font_size
		case .Small:
			it_font_size = c.sizes.small_font_size
		case .Big: 
			it_font_size = c.sizes.big_font_size
		}

		push_primitive_text(it_bounds, it.s, it_font_size, it.align)
	}
}

push_primitive_text :: proc(bounds: Rect, content: string, font_size: f32, align: Text_Align = .Middle_Center, cursor_pos := -1, cursor_opacity: f32 = 1) {
	pos: [2]f32

	text_w := c.text_width_callback(font_size, content)
	switch align {
	case .Top_Left:
		pos = {bounds.x, bounds.y}
	case .Top_Center:
		pos = {bounds.x + (bounds.w / 2 - text_w / 2), bounds.y}
	case .Top_Right:
		pos = {bounds.x + (bounds.w - text_w), bounds.y}
	
	case .Middle_Left:
		pos = {bounds.x, bounds.y + bounds.h / 2 - font_size / 2}
	case .Middle_Center:
		pos = {bounds.x + (bounds.w / 2 - text_w / 2), bounds.y + bounds.h / 2 - font_size / 2}
	case .Middle_Right:
		pos = {bounds.x + (bounds.w - text_w), bounds.y + bounds.h / 2 - font_size / 2}

	case .Bottom_Left:
		pos = {bounds.x, bounds.y + bounds.h - font_size}
	case .Bottom_Center:
		pos = {bounds.x + (bounds.w / 2 - text_w / 2), bounds.y + bounds.h - font_size}
	case .Bottom_Right:
		pos = {bounds.x + (bounds.w - text_w), bounds.y + bounds.h - font_size}
	}

	c.primitives_buffer[c.primitives_buffer_used] = Primitive {
		kind = .Text,
		u = {
			text = Primitive_Text {
				pos = pos,
				content = content,
				size = font_size,
			},
		},
	}
	c.primitives_buffer_used += 1

	if cursor_pos != -1 {
		c.primitives_buffer[c.primitives_buffer_used] = Primitive {
			kind = .Cursor,
			u = {
				cursor = Primitive_Cursor {
					pos     = pos + {c.text_width_callback(font_size, content[:cursor_pos]), 0},
					size    = font_size,
					opacity = cursor_opacity,
				},
			},
		}
		c.primitives_buffer_used += 1		
	}
}


push_primitive_rect :: proc(bounds: Rect, kind: Primitive_Kind, mouse_state: Brick_Mouse_State = .Idle, glow: f32 = 0, focus_glow: f32 = 0) {
	c.primitives_buffer[c.primitives_buffer_used] = Primitive {
		kind = kind,
		u = {
			rect = Primitive_Rect {
				bounds      = bounds,
				mouse_state = mouse_state,
				glow        = glow,
				focus_glow  = focus_glow,
			},
		},
	}
	c.primitives_buffer_used += 1
}

push_sub_kind :: proc(sub_kind: int) {
	c.widget_sub_kind_buffer = sub_kind
}

pop_sub_kind :: proc() -> (sub_kind: int) {
	sub_kind = c.widget_sub_kind_buffer
	c.widget_sub_kind_buffer = 0
	return
}

father_brick :: proc() -> ^Brick {
	if c.awating_child {
		return c.brick_p 
	} else {
		return c.brick_p.father 
	}
}

set_focus :: proc(b: ^Brick) {
	c.focused_father_brick_id = b.father != nil ? b.father.id : 0
	c.focused_brick_id = b.id
	c.focused_setted_in_frame = true
}

is_mouse_over :: #force_inline proc(b: ^Brick) -> bool {
	return c.frame.mouse_x >= b.rect.x && c.frame.mouse_x < (b.rect.x + b.rect.w) &&
		c.frame.mouse_y >= b.rect.y && c.frame.mouse_y < (b.rect.y + b.rect.h)	
}

process_keyboard_input :: proc(buffer: []rune, buf_ctx: ^Editing_Buffer_Ctx, wdata: ^WData) {
	if c.frame.text_input_buffer_used > 0 && buf_ctx.virtual_size < len(buffer) {
		for ch in c.frame.text_input_buffer[:c.frame.text_input_buffer_used] {
			for i := buf_ctx.virtual_size; i > buf_ctx.cursor; i -= 1 {
				buffer[i] = buffer[i - 1]
			}
			buffer[buf_ctx.cursor] = ch
			buf_ctx.cursor += 1
			buf_ctx.virtual_size += 1
			wdata.changed = true
		}
	}

	if c.frame.keys.backspace && buf_ctx.cursor > 0 {
		for i := buf_ctx.cursor - 1; i < (buf_ctx.virtual_size - 1); i += 1 {
			buffer[i] = buffer[i + 1]
		}
		buffer[buf_ctx.virtual_size - 1] = ' '
		buf_ctx.cursor -= 1
		buf_ctx.virtual_size -= 1
		wdata.changed = true
	}

	if c.frame.keys.left && buf_ctx.cursor > 0 {
		buf_ctx.cursor -= 1
	}

	if c.frame.keys.right && buf_ctx.cursor < buf_ctx.virtual_size {
		buf_ctx.cursor += 1
	}
}

request_left_drag :: proc(b: ^Brick, wdata_param: ^WData = nil) -> (wdata_ret: WData) {
	wdata := wdata_param != nil ? wdata_param : &wdata_ret

	if !c.prev_frame.mouse_left && c.frame.mouse_left {
		if is_mouse_over(b) {
			c.dragging_brick_id = b.id
			c.is_dragging_left  = true
			c.mouse_interacted_in_frame = true
		}
	}

	wdata.left_dragging = c.dragging_brick_id == b.id && c.is_dragging_left
	wdata.dragging_vector = c.dragging_vector

	return wdata^
}

request_focus :: proc(b: ^Brick, wdata_param: ^WData = nil) -> (wdata_ret: WData) {
	wdata := wdata_param != nil ? wdata_param : &wdata_ret

	already_focused := c.focused_brick_id == b.id

	if c.prev_frame.mouse_left && !c.frame.mouse_left {
		if is_mouse_over(b) && !c.focused_setted_in_frame {
			c.focused_father_brick_id = b.father != nil ? b.father.id : 0
			c.focused_brick_id        = b.id
			c.focused_setted_in_frame = true
			c.mouse_interacted_in_frame = true
		} else if !c.focused_setted_in_frame {
			c.focused_father_brick_id = 0
			c.focused_brick_id        = 0
		}
	}

	wdata.focused = c.focused_brick_id == b.id
	wdata.start_focus = !already_focused

	return wdata^
}

request_mouse :: proc(b: ^Brick, wdata_param: ^WData = nil) -> (wdata_ret: WData) {
	wdata := wdata_param != nil ? wdata_param : &wdata_ret

	in_bounds := is_mouse_over(b) && !c.mouse_interacted_in_frame
	mouse_down          :=  c.frame.mouse_left && !c.is_dragging_left && !c.is_dragging_right
	can_be_a_up_click   := c.prev_frame.mouse_left && !c.frame.mouse_left && !c.is_dragging_left && !c.is_dragging_right
	can_be_a_down_click := !c.prev_frame.mouse_left && c.frame.mouse_left && !c.is_dragging_left && !c.is_dragging_right

	switch b.state.mouse {
	case .Idle:
		if in_bounds {
			if can_be_a_down_click {
				b.state.mouse = .Holding
			} else if c.frame.mouse_left {
				b.state.mouse = .Fake_Holding
			} else {
				b.state.mouse = .Hovering
			}
		}
	case .Hovering:
		if can_be_a_down_click && in_bounds {
			b.state.mouse = .Holding
			wdata.down_clicked = true
		} else if in_bounds {
			b.state.mouse = .Hovering
		} else {
			b.state.mouse = .Idle
		}
	case .Clicked:
		if in_bounds {
			b.state.mouse = .Hovering
		} else {
			b.state.mouse = .Idle
		}
	case .Fake_Holding:
		if in_bounds && !c.frame.mouse_left {
			b.state.mouse = .Hovering
		} else {
			b.state.mouse = .Idle
		}
	case .Holding:
		if can_be_a_up_click && in_bounds {
			b.state.mouse = .Clicked
		} else if mouse_down && in_bounds {
			b.state.mouse = .Holding
		} else if in_bounds {
			b.state.mouse = .Hovering
		} else {
			b.state.mouse = .Idle
		}
	}

	switch b.state.mouse {
	case .Idle:
		wdata.idle = true
	case .Holding:
		wdata.holding = true
		wdata.hovering = true
	case .Hovering:
		wdata.hovering = true
	case .Clicked:
		wdata.clicked = true
		wdata.hovering = true
	case .Fake_Holding:
	}

	if in_bounds {
		wdata.mouse_event_rel_pos.x = f32(c.frame.mouse_x - b.rect.x) / f32(b.rect.w)
		wdata.mouse_event_rel_pos.y = f32(c.frame.mouse_y - b.rect.y) / f32(b.rect.h)
		c.mouse_interacted_in_frame = true
	}

	return wdata^
}

start_overlay_brick :: proc() {
	c.prev_bricks[c.prev_bricks_qnt] = c.brick_p 
	c.prev_awating_child[c.prev_bricks_qnt] = c.awating_child 
	c.prev_bricks_qnt += 1

	for it := &c.bricks[0]; true; it = it.next {
		if it.next == nil {
			it.next = new_brick(1) //TODO: Manage the ID in this case, like dont search for id
			it.next.widget_kind = .Overlay

			c.brick_p = it.next
			c.awating_child = true
			break
		}
	}
}

end_overlay_brick :: proc() {
	c.prev_bricks_qnt -= 1
	c.brick_p = c.prev_bricks[c.prev_bricks_qnt]
	c.awating_child = c.prev_awating_child[c.prev_bricks_qnt]
}

new_brick :: proc(id: u32) -> (brick: ^Brick) {
	brick  = &c.bricks[c.bricks_qnt]
	brick^ = Brick{}
	brick.id = id
	c.bricks_qnt += 1

	if id in c.state_map {
		brick.state  = c.state_map[id]^
		brick.is_old = true
	}

	return
}

gen_id :: proc(widget_kind: Widget_Kind, id: Id_Gen_Blueprint) -> (generated_id: u32) {
	to_bytes :: proc(v: $T) -> [size_of(T)]byte {
    	return transmute([size_of(T)]byte)v
	}

	mem_append :: proc(src : $T, dst : []byte, p : ^int) {
		src_in_bytes := to_bytes(src)
		for i := 0; i < size_of(T); i += 1 {
			dst[i + p^] = src_in_bytes[i]
		}
		p^ += size_of(T)
	}

	buffer: [1024]byte
	p := 0

	#partial switch id.mode {
	case .FatherLoc:
		mem_append(c.awating_child ? c.brick_p.id : c.brick_p.father.id, buffer[:], &p)
		mem_append(c.loc_buffer, buffer[:], &p)
		mem_append(widget_kind, buffer[:], &p)
		generated_id = hash.crc32(buffer[:p])
	case .FatherLocIndex:
		mem_append(c.awating_child ? c.brick_p.id : c.brick_p.father.id, buffer[:], &p)
		mem_append(c.loc_buffer, buffer[:], &p)
		mem_append(widget_kind, buffer[:], &p)
		mem_append(id.index, buffer[:], &p)
		generated_id = hash.crc32(buffer[:p])
	case .FatherLocChilduuid:
		mem_append(c.awating_child ? c.brick_p.id : c.brick_p.father.id, buffer[:], &p)
		mem_append(c.loc_buffer, buffer[:], &p)
		mem_append(widget_kind, buffer[:], &p)

		if c.awating_child {
			mem_append(c.brick_p.child_next_aux_uuid, buffer[:], &p)
			c.brick_p.child_next_aux_uuid += 1
		} else {
			mem_append(c.brick_p.father.child_next_aux_uuid, buffer[:], &p)
			c.brick_p.father.child_next_aux_uuid += 1
		}
		generated_id = hash.crc32(buffer[:p])
	}

	return
}

next_brick :: proc(widget_kind: Widget_Kind, id: Id_Gen_Blueprint, rect := Rect{}, text: string = "", sub_kind: int = 0) -> (brick: ^Brick) {
	brick = new_brick(gen_id(widget_kind, id))
	brick.widget_kind = widget_kind
	brick.rect = rect
	brick.text = text
	brick.widget_sub_kind = sub_kind

	if c.awating_child {
		brick.father = c.brick_p

		c.brick_p.children = brick
		c.brick_p = c.brick_p.children
		c.awating_child = false
	} else {
		brick.father = c.brick_p.father

		c.brick_p.next = brick
		c.brick_p = c.brick_p.next
	}
	return
}

start_brick_children :: proc() {
	c.awating_child = true
}

end_brick_children :: proc() {
	if c.awating_child {
		c.awating_child = false
	} else {
		c.brick_p = c.brick_p.father
	}
}
