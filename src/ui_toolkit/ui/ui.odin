package ui

import "core:fmt"
import "core:runtime"

BACKEND :: #config(UI_BACKEND, "custom")
MAX_LAYOUT_UNITS :: 256
MAX_BRICKS :: 1024
TEXT_INPUT_BUFFER_SIZE :: 256
MAX_OVERLAY_LAYERS :: 64

// bom dia, o sol ja nasceu la na fazendinha, acordou o bezerro e a vaquinha ja cocorico dona galinha

Rect :: struct {
	x, y, w, h: f32,
}

Keys :: struct {
	up, down, left, right, tab, esc, ctrl, delete, ctrl_c, ctrl_v, ctrl_x, backspace, enter: bool,
}

Reference_Palette :: struct {
	highlight: [4]f32,
	
}

Frame_Context :: struct {
	width, height, mouse_x, mouse_y: f32,
	scroll_axis:                     f32,
	mouse_left, mouse_right:         bool,
	text_input_buffer:               [TEXT_INPUT_BUFFER_SIZE]rune,
	text_input_buffer_used:          int,
	keys:                            Keys,
	delta:                           f32,
}

Context :: struct {
	prev_frame:                 Frame_Context,
	frame:                      Frame_Context,

	max_char_width_callback:     proc(f32) -> f32,
	text_width_callback:         proc(f32, string) -> f32,
	update_glow_callback:        proc(^f32, Brick_Mouse_State),
	update_focus_glow_callback:  proc(^f32, bool),
	get_cursor_opacity_callback: proc(f32) -> f32,
	get_translation_by_glow:     proc(f32, Text_Move_Direction) -> [2]f32,
	update_text_glow_callback:   proc(glow: ^f32, mouse_state: Brick_Mouse_State, proximity: f32) -> f32,

	brick_p:                    ^Brick,
	awating_child:              bool,
	prev_bricks:                [MAX_OVERLAY_LAYERS]^Brick,
	prev_awating_child:         [MAX_OVERLAY_LAYERS]bool,
	prev_bricks_qnt:            int,
	sizes:                      Sizes,

	layout_p:                   ^Layout_Unit,
	prev_layouts:               [MAX_OVERLAY_LAYERS]^Layout_Unit,
	prev_layouts_qnt:           int,

	bricks:                     [MAX_BRICKS]Brick,
	bricks_qnt:                 int,
	layout_units:               [MAX_LAYOUT_UNITS]Layout_Unit,

	loc_buffer:                 runtime.Source_Code_Location,
	widget_sub_kind_buffer:     int,

	state_map:                  map[u32]^Brick_State,
	states:                     [MAX_BRICKS]Brick_State,
	states_qnt:                 int,
	
	is_dragging_left:           bool,
	is_dragging_right:          bool,
	dragging_vector:            [2]f32,
	dragging_brick_id:          u32,

	focused_father_brick_id:    u32,
	focused_brick_id:           u32,
	focused_setted_in_frame:    bool,

	text_padding_inside_rect:   f32,     

	mouse_interacted_in_frame:  bool,

	primitives_buffer:          [MAX_BRICKS * 8]Primitive,
	primitives_buffer_used:     int,

	uptime:                     f32,
}

// some aliases to not internal use
split :: split_row
next :: next_column
pop :: pop_layout

c: ^Context

set_loc :: #force_inline proc(loc: runtime.Source_Code_Location) {
	c.loc_buffer = loc
}

all_buffered_primitives :: #force_inline proc() -> []Primitive {
	return c.primitives_buffer[:c.primitives_buffer_used]
}

start_overlay :: proc() {
	start_overlay_layout()
	start_overlay_brick()
}

end_overlay :: proc() {
	end_overlay_brick()
	end_overlay_layout()
}

init_context :: proc(c: ^Context) {
	c.state_map = make(map[u32]^Brick_State)
	c.sizes = DEFAULT_SIZES
	c.update_glow_callback = default_update_glow_callback
	c.update_focus_glow_callback = default_update_focus_glow_callback
	c.get_cursor_opacity_callback = default_get_cursor_opacity_callback
	c.get_translation_by_glow = default_get_translation_by_glow
}

set_context :: proc(new_c: ^Context) {
	c = new_c
}

init_frame :: proc(frame_ctx: Frame_Context) {
	c.frame = frame_ctx

	c.focused_setted_in_frame   = false
	c.mouse_interacted_in_frame = false

	if c.is_dragging_left || c.is_dragging_right {
		c.dragging_vector.x = c.frame.mouse_x - c.prev_frame.mouse_x
		c.dragging_vector.y = c.frame.mouse_y - c.prev_frame.mouse_y
	}

	c.awating_child = true

	for i := 0; i < MAX_LAYOUT_UNITS; i += 1 {
		c.layout_units[i].active = false
	}
	c.layout_p = new_layout_unit()
	c.layout_p.rect = Rect{ w=frame_ctx.width, h=frame_ctx.height }
	c.layout_p.fixed_height = true

	// for while, clean all bricks

	c.bricks_qnt = 0
	c.brick_p = new_brick(1)
	c.brick_p.widget_kind = .Root

	c.primitives_buffer_used = 0

	split_row(0, P(1.0))
	fix_a_size_column_by_remaining(0)
}

end_frame :: proc() {
	c.uptime += c.frame.delta

	if !c.frame.mouse_left {
		c.is_dragging_left = false
	}

	if !c.frame.mouse_right {
		c.is_dragging_right = false
	}

	c.prev_frame = c.frame

	delete(c.state_map)
	c.state_map = make(map[u32]^Brick_State)
	for i := 0; i < c.bricks_qnt; i += 1 {
		c.states[i] = c.bricks[i].state
		c.state_map[c.bricks[i].id] = &c.states[i]
	}
	c.states_qnt = c.bricks_qnt
}

print_bricks :: proc(detailed := true) {
	_print_tabs :: proc(tabs: int) {
		tabs_string := "                                                                                                    "
		fmt.printf("%s", tabs_string[:tabs * 8])
	}

	_print_brick :: proc(tabs: int, b: ^Brick, ident_kind: bool, detailed: bool) {
		if b != nil {
			if ident_kind {
				_print_tabs(tabs)			
			}
			fmt.println(b.widget_kind)

			if detailed {
				_print_tabs(tabs)
				fmt.println("|")
				_print_tabs(tabs)
				fmt.println("| ", b)
				_print_tabs(tabs)
				fmt.println("|")
			}

			if b.children != nil {
				_print_tabs(tabs)
				fmt.println("|  children")
				_print_tabs(tabs)
				fmt.print("|------>")

				_print_brick(tabs + 1, b.children, false, detailed)
			}

			if b.next != nil {
				_print_tabs(tabs)
				fmt.println("|  next")
				_print_tabs(tabs)
				fmt.println("V")

				_print_brick(tabs, b.next, true, detailed)
			}
		}
	}

	fmt.println("=== INIT BRICKS ===")
	_print_brick(0, &c.bricks[0], true, detailed)
	fmt.printf("Qnt: %d\n", c.bricks_qnt)
	fmt.println("=== END  BRICKS ===")
}

print_state :: proc() {
	fmt.println("=== INIT STATE MAP ===")
	qnt := 0
	for key, value in c.state_map {
		fmt.printf("Id: %d State: ", key)
		fmt.println(value)
		qnt += 1
	}
	fmt.printf("Qnt: %d\n", qnt)
	fmt.println("=== END STATE MAP ===")
}

my_bprint_int :: proc(buffer: []byte, leading_zeroes: i8, value: int) -> int {
	p := len(buffer)
	v := value

	if value < 0 {
		v = -v
	}

	for {
		remainder := v % 10
		v /= 10
		p -= 1
		buffer[p] = '0' + byte(remainder)
		//fmt.println("ch =", buffer[p])
		if v == 0 {
			break
		}
	}

	for _ in 0..<leading_zeroes {
		p -= 1
		buffer[p] = '0'
		//fmt.println("ch =", buffer[p])
	}

	if value < 0 {
		p -= 1
		buffer[p] = '-'
	}

	return p
}

my_atoi :: proc(buffer: []byte) -> (value: int) {
	m := 1
	for p := len(buffer) - 1; p >= 1; p -= 1 {
		value += int(buffer[p] - '0') * m
		m *= 10
	}
	if buffer[0] != '-' {
		value += int(buffer[0] - '0') * m
	} else {
		value *= -1
	}
	return
}

// some util vectors

leading_zeroes_text := [?]string{
	"",
	"0",
	"00",
	"000",
	"0000",
	"00000",
	"000000",
	"0000000",
	"00000000",
	"000000000",
	"0000000000",
	"00000000000",
	"000000000000",
	"0000000000000",
	"00000000000000",
}