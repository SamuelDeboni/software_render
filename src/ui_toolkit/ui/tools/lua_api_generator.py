import sys
import re
import datetime

DEFAULT_ID_MACRO = "DEFID"
CALLER_LOCATION_MACRO = "#caller_location"
ID_TYPE = "Id_Gen_Blueprint"
CALLER_LOCATION_TYPE = "runtime.Source_Code_Location"
HEADER = """/**
 * Auto generated file, please dont modify.
 * {}
 */
""".format(str(datetime.datetime.now()))

ODIN_VARIABLE = "="
ODIN_CONST = ":"
ODIN_C_CONV = '"c"'

class OdinFileBuilder:
    def __init__(self):
        self.tabs = 0
        self.output = HEADER

    def add_package(self, name):
        self.output += "package {}\n".format(name)

    def add_import(self, path, alias = ""):
        self.output += 'import {} "{}"\n'.format(alias, path)

    def join_parameters(self, params):
        return "".join([(", " if i != 0 else "") + p for i, p in enumerate(params)])

    def new_proc(self, name, params, to_return = [], convention = "", attr_or_equals = ODIN_CONST):
        params_str = self.join_parameters(params)
        to_return_str = self.join_parameters(to_return)
        self.output += ("\t" * self.tabs) + "{} :{} proc {} ({}) {} {{\n".format(
            name, attr_or_equals, convention, params_str, "-> ("+to_return_str+")" if len(to_return) > 0 else "")
        self.tabs += 1

    def add_simple_def_call(self, name, type, to_call, to_call_params, cast = ""):
        to_call_params_str = self.join_parameters(to_call_params)
        self.output += ("\t" * self.tabs) + "{}: {} = {}({}({}))\n".format(name, type, cast, to_call, to_call_params_str)

    def add_simple_def(self, name, type, value):
        self.output += ("\t" * self.tabs) + "{}: {} = {}\n".format(name, type, value)

    def add_simple_attr_call(self, name, to_call, to_call_params, cast = ""):
        to_call_params_str = self.join_parameters(to_call_params)
        self.output += ("\t" * self.tabs) + "{} = {}({}({}))\n".format(name, cast, to_call, to_call_params_str)

    def add_simple_call(self, to_call, to_call_params):
        to_call_params_str = self.join_parameters(to_call_params)
        self.output += ("\t" * self.tabs) + "{}({})\n".format(to_call, to_call_params_str)

    def add_return(self, to_return):
        self.output += ("\t" * self.tabs) + "return {}\n".format(to_return)

    def add_single_line_comment(self, comment):
        self.output += ("\t" * self.tabs) + "//{}\n".format(comment)

    def end_proc(self):
        self.tabs -= 1
        self.output += ("\t" * self.tabs) + "}\n"

    def new_line(self):
        self.output += "\n"

    def to_string(self):
        return self.output

class Procedure:
    def __init__(self, name, params):
        self.name = name
        self.params = params

class ProcedureParam:
    def __init__(self, name, type):
        self.name = name
        self.type = type

def check_int(s):
    if s[0] in ('-', '+'):
        return s[1:].isdigit()
    return s.isdigit()

def main():
    widgets_path     = sys.argv[1]
    lua_package_path = sys.argv[2]
    output_path      = sys.argv[3]

    # ------------------------------
    # Collect procedures definitions
    # ------------------------------

    procedures = []
    ignore_next = False

    IGNORE_TAG_REGEX = "\!lua_ignore"
    PROC_REGEX = "::.*proc\("
    PROC_PARAM_REGEX = "(\w+(, *\w+ *)* *\:\=? *((\[(\d|\w)*\])|\^|\#|(\.\.))?\w+)"

    for line in open(widgets_path).read().split("\n"):
        if re.search(PROC_REGEX, line):
            if ignore_next:
                ignore_next = False
                continue

            procedure_name = line.split("::")[0].strip()
            params_reg = re.findall(PROC_PARAM_REGEX, line.split(")")[0].split("(")[1])        
            params = []
            
            for param_str in [p[0] for p in params_reg]:
                param_names = [it.strip() for it in param_str.split(":")[0].strip().split(",")]
                param_type = ""
                
                if not re.search("\:\=", param_str):
                    param_type = re.findall("(((\[(\d|\w)*\])|\^|\#|(\.\.))?\w+)", param_str.split(":")[1].strip())[-1][0]
                else:
                    value = re.findall("#?\w+", param_str.split(":")[1].strip())[-1]
                    if value == DEFAULT_ID_MACRO:
                        param_type = ID_TYPE
                    elif value == CALLER_LOCATION_MACRO:
                        param_type = CALLER_LOCATION_TYPE
                    elif value == "true" or value == "false":
                        param_type = "bool"
                    elif check_int(value):
                        param_type = "int"
                
                for name in param_names:
                    params.append(ProcedureParam(name, param_type))

            procedures.append(Procedure(procedure_name, params))
        elif re.search(IGNORE_TAG_REGEX, line):
            ignore_next = True

    # -------------------
    # Generate the output
    # -------------------

    odin_builder = OdinFileBuilder()
    odin_builder.add_package("ui")
    odin_builder.new_line()
    odin_builder.add_import("core:fmt")
    odin_builder.add_import("core:c")
    odin_builder.add_import("core:runtime")
    odin_builder.add_import(lua_package_path, "l")
    odin_builder.new_line()
    odin_builder.new_proc("bind_ui_functions", ["L: ^l.lua_State"])
    odin_builder.add_simple_call("l.lua_createtable", ["L", "0", "0"])
    odin_builder.new_line()

    output = ""
    for proc in procedures:
        odin_builder.add_single_line_comment(proc.name)
        odin_builder.new_proc("_lua_"+proc.name, ["L: ^l.lua_State"], ["c.int"], ODIN_C_CONV, ODIN_VARIABLE)
        odin_builder.add_simple_attr_call("context", "runtime.default_context", [])
        odin_builder.new_line()

        var_counter = 0
        for param in proc.params:
            if param.type == "int" or param.type == "i32" or param.type == "f32":
                odin_builder.add_simple_def_call(param.name, param.type, "l.luaL_checknumber", ["L", str(var_counter + 1)], param.type)
            elif param.type == "string":
                odin_builder.add_simple_def(param.name+"_size", "c.ptrdiff_t", "0")
                odin_builder.add_simple_def_call(param.name, param.type, "l.luaL_checklstring", ["L", str(var_counter + 1), "&"+param.name+"_size"], "string")
            elif param.type == "bool":
                odin_builder.add_simple_def_call(param.name, param.type, "l.lua_toboolean", ["L", str(var_counter + 1)], "bool")
            elif param.type == ID_TYPE:
                odin_builder.add_simple_def(param.name, param.type, DEFAULT_ID_MACRO)
            elif param.type == CALLER_LOCATION_TYPE:
                odin_builder.add_simple_def(param.name+"_debug_info", "l.lua_Debug", "{}")
                odin_builder.add_simple_call("l.lua_getfield", ["L", 'l.LUA_REGISTRYINDEX', '"f"'])
                odin_builder.add_simple_call("l.lua_getinfo", ["L", '">Sln"', "&"+param.name+"_debug_info"])
                odin_builder.add_simple_def(param.name, CALLER_LOCATION_TYPE, "{}{{ line = {} }}".format(CALLER_LOCATION_TYPE, param.name+"_debug_info.currentline"))
            else:
                odin_builder.add_simple_def(param.name, param.type, "{}")

            var_counter += 1

        odin_builder.new_line()
        
        odin_builder.add_simple_call(proc.name, [p.name for p in proc.params])
        odin_builder.add_return("0")
        odin_builder.end_proc()
        
        odin_builder.add_simple_call("l.lua_pushcfunction", ["L", "_lua_"+proc.name])
        odin_builder.add_simple_call("l.lua_setfield", ["L", "-2", '"{}"'.format(proc.name)])
        odin_builder.new_line()
    odin_builder.new_line()
    odin_builder.add_simple_call("l.lua_setglobal", ["L", '"ui"'])
    odin_builder.end_proc()

    output_file = open(output_path, "w")
    output_file.write(odin_builder.to_string())
    output_file.close()


# just call the main procedure
main()