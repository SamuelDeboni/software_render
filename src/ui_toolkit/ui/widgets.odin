package ui

import "core:fmt"
import "core:strconv"
import "core:math"
import utf8 "core:unicode/utf8"

Widget_Kind :: enum {
	Animation,
	Root,
	Overlay,
	Card,
	MenuBar,
	MenuButton,
	RadioGroup,
	Button,
	Icon_Button,
	Badge,
	Label,
	Image,
	TooltipCard,
	CheckboxMarker,
	Textfield,
	SliderBar,
	SliderBullet,
	SliderClicklableArea,
	NodeHeader,
	NodeLabelLink,
	NodeInputBullet,
	NumberInputField,
	NumberInputUpButton,
	NumberInputDownButton,
	SelectInputField,
	Dropdown,
	Alert_Dropdown,
	Popup,
	Tab,
	TabBar,
	TextareaLineIndication,
	TextareaLineData,
	TextareaEditingInfo,
	TextareaCard,
	SidePanel,
	MenuButton2,
	Extra,
	Panel,
	EditorLine,
	Dummy,
}

Sizes :: struct {
	normal_line_height: f32,
	thin_line_height:   f32,
	basic_padding:      f32,
	big_font_size:      f32,
	normal_font_size:   f32,
	small_font_size:    f32,
}

DEFAULT_SIZES :: Sizes {
	normal_line_height = 30,
	thin_line_height   = 20,
	basic_padding      = 4,
	big_font_size      = 30,
	normal_font_size   = 20,
	small_font_size    = 10,
}

//-----------------------------------------------------------------------------
// Base components
//-----------------------------------------------------------------------------

button_ex :: proc(text_blueprints: []Text_Draw_Blueprint, height: f32, font_size: f32, primitive_kind: Primitive_Kind, color: [4]f32, id := DEFID, loc := #caller_location) -> WData {
	set_loc(loc)

	//brick := next_brick(.Button, id, reserve_line(height, c.text_width_callback(font_size, name)), name)
	brick := next_brick(.Button, id, reserve_line(height, 300)) //TODO: get an way to pass the text, maybe replace string for []Text_Draw_Blueprint

	wdata := request_mouse(brick)

	c.update_glow_callback(&brick.state.glow, brick.state.mouse)

	push_primitive_rect(brick.rect, primitive_kind, brick.state.mouse, brick.state.glow)
	push_blueprint_texts(brick.rect, text_blueprints, brick.state.glow)

	return wdata
}

label_ex :: proc(text: string, height: f32, font_size: f32, color: [4]f32, id := DEFID, loc := #caller_location) {
	set_loc(loc)
	brick := next_brick(.Label, id, reserve_line(height, c.text_width_callback(font_size, text)), text)

	push_primitive_text(brick.rect, text, font_size, .Middle_Left)
}

textfield_ex :: proc(buffer: []rune,  buf_ctx: ^Editing_Buffer_Ctx = nil, placeholder: string = "", height: f32, font_size: f32, id := DEFID, loc := #caller_location) -> (wdata: WData) {
	set_loc(loc)

	cursor_blink := false

	brick := next_brick(.Textfield, id, reserve_line(height, 300))	
	if request_focus(brick, &wdata).focused {
		process_keyboard_input(buffer, buf_ctx, &wdata)
		if !wdata.changed {
			cursor_blink = true
		}
	}
	brick.text = utf8.runes_to_string(buffer[:buf_ctx.virtual_size])
	brick.state.cursor = buf_ctx.cursor

	buf_ctx.str = brick.text
	wdata.str = brick.text
	wdata.error = &brick.state.error
	wdata.error_msg = &brick.state.error_msg

	c.update_glow_callback(&brick.state.glow, brick.state.mouse)
	c.update_focus_glow_callback(&brick.state.focus_glow, wdata.focused)

	push_primitive_rect(brick.rect, .Rect_Input, brick.state.mouse, brick.state.glow, brick.state.focus_glow)

	cursor_opacity := f32(1)
	if cursor_blink {
		cursor_opacity = c.get_cursor_opacity_callback(brick.state.cursor_active_uptime)
		brick.state.cursor_active_uptime += c.frame.delta
	} else {
		brick.state.cursor_active_uptime = 0
	}

	text_rect := brick.rect
	text_rect.x += (height - font_size) / 2

	if len(buf_ctx.str) <= 0 && len(placeholder) > 0 && !wdata.focused {
		push_primitive_text(text_rect, placeholder, font_size, .Middle_Left)
	} else {
		push_primitive_text(text_rect, brick.text, font_size, .Middle_Left, brick.state.cursor, cursor_opacity)
	}

	return wdata
}

checkbox_ex :: proc(size: f32, id := DEFID, loc := #caller_location) -> (wdata: WData) {
	set_loc(loc)

	split_row(LEFT, DYNAMIC_W, FILL_W)
	{
		brick := next_brick(.Button, id, reserve_line(size, size))

		if request_mouse(brick, &wdata).clicked {
			brick.state.checked = !brick.state.checked
			wdata.changed = true
		}

		wdata.checked = bool(brick.state.checked)

		c.update_glow_callback(&brick.state.glow, brick.state.mouse)

		push_primitive_rect(brick.rect, brick.state.checked ? .Rect_Marker_Checked : .Rect_Marker_Unchecked, brick.state.mouse, brick.state.glow)
	}
	pop_layout()

	return wdata
}

/*
new_card :: proc(id := DEFID, loc := #caller_location) {
	set_loc(loc)

	//split_row(0, 4, FILL_W, 4); next_column()
	split_row(0, FILL_W)

	space(4)

	next_brick(.Card, id)
	start_brick_children()
}

end_card :: proc() {
	space(4)

	end_brick_children()

	//pop_layout()
	c.brick_p.rect = pop_layout().rect
} */

new_container_ex :: proc(id := DEFID, loc := #caller_location) {
	set_loc(loc)

	next_brick(.Card, id)
	start_brick_children()

	split_row(0, 4, FILL_W, 4)
	next_column()

	space(4)
}

end_container_ex :: proc() {
	space(4)

	end_brick_children()
	c.brick_p.rect = pop_layout().rect
}

//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------

line_break :: proc() {
	space(c.sizes.normal_line_height)
}

button :: proc(name: string, id := DEFID, loc := #caller_location) -> WData {
	return button_ex({{s = name}}, c.sizes.normal_line_height, c.sizes.normal_font_size, .Rect_Button, {}, id, loc)
}

button_tab_left :: proc(name: string, id := DEFID, loc := #caller_location) -> WData {
	return button_ex({{s = name, align = .Middle_Left, move_dir = .Right, padding = {15, 0, 0, 0}}, {s = "podcast", align = .Middle_Right, move_dir = .Right, padding = {0, 0, 15, 0} }}, c.sizes.normal_line_height, c.sizes.normal_font_size, .Rect_Tabbed_Left, {}, id, loc)
}

label :: proc(text: string, id := DEFID, loc := #caller_location) {
	label_ex(text, c.sizes.normal_font_size, c.sizes.normal_font_size, {}, id, loc)
}

textfield :: proc(buffer: []rune,  buf_ctx: ^Editing_Buffer_Ctx = nil, placeholder: string = "", id := DEFID, loc := #caller_location) -> (wdata: WData) {
	return textfield_ex(buffer, buf_ctx, placeholder, c.sizes.normal_line_height, c.sizes.normal_font_size, id, loc)
}

checkbox :: proc(id := DEFID, loc := #caller_location) -> (wdata: WData) {
	return checkbox_ex(c.sizes.normal_line_height, id, loc)
}

//

/*

color_pin :: proc(color: [3]u8, id := DEFID, loc := #caller_location) {
	set_loc(loc)
	//next_brick(.Image, id, reserve_line(size.x, size.y), fmt.aprintf(title_to_format, ..title_args))
}

//image

image :: proc(resource_handle: i32, size: [2]f32, id := DEFID, loc := #caller_location) {
	set_loc(loc)

	brick := next_brick(.Image, id, reserve_line(size.y, size.x))
	brick.resource_handle = resource_handle
	brick.state.res_size = size
}

icon_button :: proc(resource_handle: i32, id := DEFID, loc := #caller_location) -> WData {
	set_loc(loc)

	brick := next_brick(.Icon_Button, id, reserve_line(20, 20))
	brick.resource_handle = resource_handle
	brick.state.res_size = [2]f32{20, 20}

	return request_mouse(brick)
}

//slider

slider_int :: proc(init, end: int, force_value := false, forced_value := 0, step := 1, id := DEFID, loc := #caller_location) -> (int) {
	set_loc(loc)

	rect := reserve_line(20, 300)

	clickable := next_brick(.SliderClicklableArea, id)
	clickable.rect = rect

	if force_value {
		clickable.state.value_int = forced_value
	}

	wdata := request_mouse(clickable)
	if clickable.state.mouse == .Clicked || clickable.state.mouse == .Holding {
		clickable.state.value_int = int(wdata.mouse_event_rel_pos.x * f32(end + 1 - init)) + init
	}
	
	bar := next_brick(.SliderBar, id)
	bar.rect = Rect{
		x = rect.x + 10,
		y = rect.y + 7,
		w = rect.w - 20,
		h = rect.h - 14,
	}

	bullet := next_brick(.SliderBullet, id)
	bullet.rect = Rect{
		x = rect.x + ((rect.w - 20) / f32(end - init) * f32(clickable.state.value_int)),
		y = rect.y,
		w = 20,
		h = rect.h,
	}
	bullet.text = fmt.aprint(clickable.state.value_int)

	return clickable.state.value_int
}

//text field

new_radio_group :: proc(id := DEFID, loc := #caller_location) {
	set_loc(loc)

	next_brick(.RadioGroup, id)
	start_brick_children()
}

radio :: proc(name: string, id := DEFID, loc := #caller_location) -> (interacted: bool, marked: bool) {
	set_loc(loc)

	split_row(NO_LOOP|LEFT, 20, FILL_W)
	{
		rect := reserve_line(20, 20)
		brick := next_brick(.CheckboxMarker, id)
		brick.rect = rect
		brick.text = name
		request_mouse(brick)
		if brick.state.mouse == .Clicked {
			interacted = true
			if brick.father.state.value_int == (brick.father.temporary_int + 1) {
				brick.father.state.value_int = 0
				if Brick_State_Flags.Checked in brick.state.flags {
					excl(&brick.state.flags, Brick_State_Flags.Checked)
				}
				marked = false
			} else {
				brick.father.state.value_int = brick.father.temporary_int + 1
				marked = true
			}
		}

		if brick.father.state.value_int == (brick.father.temporary_int + 1) {
			marked = true
			if Brick_State_Flags.Checked not_in brick.state.flags {
				incl(&brick.state.flags, Brick_State_Flags.Checked)
			}
		} else {
			if Brick_State_Flags.Checked in brick.state.flags {
				excl(&brick.state.flags, Brick_State_Flags.Checked)
			}
		}
		brick.father.temporary_int += 1

		pop_layout()
	}

	return
}

end_radio_group :: proc() {
	end_brick_children()
}

checkbox :: proc(text: string = "", id := DEFID, loc := #caller_location) {
	set_loc(loc)

	//split_row(false, .Left, .Px, DYNAMIC_W)
	split_row(NO_LOOP|LEFT, 20, DYNAMIC_W)

	rect: Rect
	brick: ^Brick
	
	rect = reserve_line(20, 20)
	brick = next_brick(.CheckboxMarker, id)
	brick.rect = rect
	request_mouse(brick)
	if brick.state.mouse == .Clicked {
		if Brick_State_Flags.Checked in brick.state.flags {
			tmp := Brick_State_Flags.Checked
			excl(&brick.state.flags, tmp)
		} else {
			incl(&brick.state.flags, Brick_State_Flags.Checked)
		}
	}
	next_column()

	rect = reserve_line(20, c.text_width_callback(c.sizes.normal_font_size, text))
	brick = next_brick(.Label, id)
	brick.rect = rect
	brick.text = text
	next_column()

	//pop_layout()
}

/*
color_checkbox :: proc(color: [3]u8, id := DEFID, loc := #caller_location) -> bool {
	set_loc(loc)

	rect: Rect
	brick: ^Brick
	
	rect = reserve_line(20, 20)
	brick = next_brick(.Badge, id)
	brick.rect = rect
	request_mouse(brick)
	if brick.state.mouse == .Clicked {
		if Brick_State_Flags.Checked in brick.state.flags {
			tmp := Brick_State_Flags.Checked
			excl(&brick.state.flags, tmp)
		} else {
			incl(&brick.state.flags, Brick_State_Flags.Checked)
		}
	}

	brick.state.color = [4]u8{color.r, color.g, color.b, 255}

	return Brick_State_Flags.Checked in brick.state.flags
} */


//select

select :: proc(options: []string, id := DEFID, loc := #caller_location) {
	set_loc(loc)

	brick := next_brick(.SelectInputField, id)
	rect := reserve_line(20, c.text_width_callback(c.sizes.normal_font_size, options[brick.state.value_int]))

	brick.rect = rect
	brick.text = options[brick.state.value_int]
	request_mouse(brick)
	if brick.state.mouse == .Clicked {
		if Brick_State_Flags.Checked in brick.state.flags {
			excl(&brick.state.flags, Brick_State_Flags.Checked)
		} else {
			incl(&brick.state.flags, Brick_State_Flags.Checked)
		}
	}

	if Brick_State_Flags.Checked in brick.state.flags {
		{
			start_overlay()

			next_brick(.Dropdown, id)
			start_brick_children()

			float_column(Rect{ x=rect.x, y=rect.y + rect.h, w=rect.w, h=0 }, true)

			space(c.sizes.basic_padding)
			split_row(0, c.sizes.basic_padding, DYNAMIC_W, c.sizes.basic_padding)
			next_column()
			split_row(0, DYNAMIC_W)
		}

		for opt, i in options {
			if i == brick.state.value_int {
				continue
			}

			if i != 0 {
				space(5)
			}
			
			rect := reserve_line(20, c.text_width_callback(c.sizes.normal_font_size, opt))
			opt_btn := next_brick(.Label, INCID)
			opt_btn.rect = rect
			opt_btn.text = opt
			request_mouse(opt_btn)
			if opt_btn.state.mouse == .Clicked {
				brick.state.value_int = i
				excl(&brick.state.flags, Brick_State_Flags.Checked)
				break
			}
		}

		{
			end_brick_children()

			pop_layout()
	
			space(c.sizes.basic_padding)
			pop_layout()
			c.brick_p.rect = c.layout_p.rect

			end_overlay()
		}
	}
}

new_dropdown :: proc(id := DEFID, loc := #caller_location) {
	rect := reserve_line(0, 200)
	
	{
		start_overlay()

		next_brick(.Dropdown, id)
		start_brick_children()

		float_column(Rect{ x=rect.x, y=rect.y + rect.h, w=rect.w, h=0 }, true)

		split_row(0, DYNAMIC_W)
		split_row(0, DYNAMIC_W)
	}	
}

end_dropdown :: proc() {
	end_brick_children()

	pop_layout()
	pop_layout()

	c.brick_p.rect = c.layout_p.rect

	end_overlay()
}

new_global_alert :: proc(id := DEFID, loc := #caller_location) {
	rect := Rect{
		x=c.layout_p.rect.x + c.sizes.basic_padding / 2,
		y=c.layout_p.rect.y + c.sizes.basic_padding / 2,
		w=c.layout_p.rect.w - c.sizes.basic_padding,
		h=0,
	}
	
	{
		start_overlay()

		next_brick(.Alert_Dropdown, id)
		start_brick_children()

		float_column(rect)

		space(c.sizes.basic_padding)
		split_row(0, c.sizes.basic_padding, FILL_W, c.sizes.basic_padding)
		next_column()
		split_row(0, FILL_W)
	}	
}

end_global_alert :: proc() -> (wdata: WData) {
	end_brick_children()

	pop_layout()

	space(c.sizes.basic_padding)
	pop_layout()
	c.brick_p.rect = c.layout_p.rect
	wdata = request_mouse(c.brick_p)

	end_overlay()
	return
}

new_popup :: proc(id := DEFID, loc := #caller_location) {
	w: f32 = 600
	rect := Rect{
		x=c.layout_p.rect.x + c.sizes.basic_padding / 2 + w / 2,
		y=c.layout_p.rect.y + c.sizes.basic_padding / 2,
		w=w - c.sizes.basic_padding,
		h=0,
	}
	
	{
		start_overlay()
		new_from_top_animation()

		next_brick(.Popup, id)
		start_brick_children()

		float_column(rect)

		split_row(0, c.sizes.basic_padding / 2, FILL_W, c.sizes.basic_padding / 2)
		next_column()
		split_row(0, FILL_W)
		space(c.sizes.basic_padding / 2)
	}	
}

end_popup :: proc() -> (wdata: WData) {
	end_brick_children()

	space(c.sizes.basic_padding / 2)
	
	pop_layout()
	pop_layout()
	
	c.brick_p.rect = c.layout_p.rect
	wdata = request_mouse(c.brick_p)

	end_from_top_animation()
	end_overlay()
	return
}

//tooltip

new_tooltip :: proc(id := DEFID, loc := #caller_location) {
	start_overlay()

	next_brick(.TooltipCard, id)
	start_brick_children()

	float_column(Rect{ x=c.frame.mouse_x, y=c.frame.mouse_y, w=0, h=0 }, true)

	space(c.sizes.basic_padding)
	split_row(0, c.sizes.basic_padding, DYNAMIC_W, c.sizes.basic_padding)
	next_column()
	split_row(0, DYNAMIC_W)
}

end_tooltip :: proc() {
	end_brick_children()

	pop_layout()
	
	space(c.sizes.basic_padding)
	pop_layout()
	c.brick_p.rect = c.layout_p.rect

	end_overlay()
}

new_float :: proc(pos: [2]f32, id := DEFID, loc := #caller_location) {
	start_overlay()

	next_brick(.TooltipCard, id)
	start_brick_children()

	float_column(Rect{ x=pos.x, y=pos.y, w=0, h=0 }, true)
}

end_float :: proc() {
	end_brick_children()

	c.brick_p.rect = pop_layout().rect

	end_overlay()
}

//side panel

new_side_panel :: proc(id := DEFID, loc := #caller_location) {
	set_loc(loc)

	next_brick(.SidePanel, id)
	start_brick_children()

	fix_a_size_column_by_remaining(0)
}

end_side_panel :: proc() {
	end_brick_children()

	c.brick_p.rect = c.layout_p.rect
}

new_card :: proc(id := DEFID, loc := #caller_location) {
	set_loc(loc)

	//split_row(0, 4, FILL_W, 4); next_column()
	split_row(0, FILL_W)

	space(4)

	next_brick(.Card, id)
	start_brick_children()
}

end_card :: proc() {
	space(4)

	end_brick_children()

	//pop_layout()
	c.brick_p.rect = pop_layout().rect
}

new_panel :: proc(id := DEFID, loc := #caller_location) {
	set_loc(loc)

	next_brick(.Panel, id)

	start_brick_children()
}

end_panel :: proc() {
	end_brick_children()

	c.brick_p.rect = c.layout_p.rect
}

new_horizontal_line :: proc(id := DEFID, loc := #caller_location) {
	set_loc(loc)
	next_brick(.EditorLine, id)

	split_row(0, FILL_W)

	start_brick_children()
}

end_horizontal_line :: proc() {
	end_brick_children()

	c.brick_p.rect = pop_layout().rect
}

//!lua_ignore
mouse_labelf :: proc(cursor: int, to_format: string, args: ..any, id := DEFID, loc := #caller_location) -> WData {
	set_loc(loc)
	sk := pop_sub_kind()
	brick: ^Brick

	formatted := fmt.aprintf(to_format, ..args)
	brick = next_brick(.Label, id, reserve_line(20, c.text_width_callback(c.sizes.normal_font_size, formatted)), formatted, sk)
	brick.state.cursor = cursor
	
	return request_mouse(brick)
}

new_inertia_animation :: proc(id := DEFID, loc := #caller_location) {
	set_loc(loc)

	brick := next_brick(.Animation, id, reserve_line(0, 0))

	brick_pos := [2]f32{brick.rect.x, brick.rect.y}
	if brick.state.cached_pos != brick_pos {
		brick.state.scroll_offset = [2]f32{
			brick.state.cached_pos.x - brick_pos.x,
			brick.state.cached_pos.y - brick_pos.y,
		}
		brick.state.inv_opacity = 1
	} else {
		brick.state.scroll_offset *= 0.9
		brick.state.inv_opacity *= 0.9
	}

	brick.state.cached_pos = [2]f32{brick.rect.x, brick.rect.y}

	start_brick_children()	
}

end_inertia_animation :: proc() {
	end_brick_children()
}

new_from_top_animation :: proc(id := DEFID, loc := #caller_location) {
	set_loc(loc)

	brick := next_brick(.Animation, id, reserve_line(0, 0))

	if brick.is_old {
		brick.state.scroll_offset /= 1.5
	} else {
		brick.state.scroll_offset = [2]f32{0, -500}
	}

	start_brick_children()
}

end_from_top_animation :: proc() {
	end_brick_children()
}

new_bottom_anchored :: proc(id := DEFID, loc := #caller_location) {
	rect := reserve_line(0, 0)
	pos := [2]f32{rect.x, c.layout_p.rect.h + c.layout_p.rect.y}

	start_overlay()
	float_column(Rect{ x=pos.x, y=pos.y, w=rect.w, h=0 })

	next_brick(.Animation, id)
	start_brick_children()
}

end_bottom_anchored :: proc() {
	end_brick_children()

	computed_height := pop_layout().rect.h
	c.brick_p.state.scroll_offset.y = -computed_height

	end_overlay()

	reduce_size_column(computed_height)

	return
}

new_dummy :: proc(id := DEFID, loc := #caller_location) -> (b: ^Brick) {
	set_loc(loc)
	split_row(0, FILL_W)

	b = next_brick(.Dummy, id)
	start_brick_children()
	return b
}

end_dummy :: proc() {
	end_brick_children()
	c.brick_p.rect = pop_layout().rect
} */