package ui_toolkit

import rl "vendor:raylib"
import "ui"
import "core:strings"
import "core:log"

@(private)
default_font: rl.Font

DEFAULT_FONT_SIZE :: 20

@(private)
font_data := #load("default_font.ttf")

create_window :: proc(width, height: int, title: string, ctx: ^ui.Context) {
	ctitle := strings.clone_to_cstring(title, context.temp_allocator)
	rl.InitWindow(i32(width), i32(height), ctitle)
	rl.SetTargetFPS(60)
	rl.SetExitKey(.NULL)
	//rl.SetWindowState({.WINDOW_RESIZABLE})

	default_font = rl.LoadFontFromMemory(".ttf", raw_data(font_data), cast(i32)len(font_data), DEFAULT_FONT_SIZE, nil, 0);

	ui.init_context(ctx)
	init_raylib_backend(ctx)
}

init_frame :: proc() {
	init_frame_raylib()
}

end_frame :: proc() {
	ui.end_frame()
	rl.BeginDrawing()
	rl.ClearBackground(rl.DARKGRAY)
	raylib_render_default()
	rl.EndDrawing()
}

window_should_close :: proc() -> bool {
	return rl.WindowShouldClose()
}

// ==== Raylib backend ====

init_raylib_backend :: proc(ctx: ^ui.Context) {
	ui.init_context(ctx)

	ctx.max_char_width_callback = proc(font_size: f32) -> f32 {
		return font_size
	}

	ctx.text_width_callback = proc(font_size: f32, text: string) -> f32 {
		if len(text) <= 0 {
			return 0
		}
		cstr := strings.clone_to_cstring(text, context.temp_allocator)
		return rl.MeasureTextEx(default_font, cstr, DEFAULT_FONT_SIZE, 2).x
	}

	ui.set_context(ctx)
}

init_frame_raylib :: proc() {
	ui_frame := ui.Frame_Context{
		width       = f32(rl.GetScreenWidth()),
		height      = f32(rl.GetScreenHeight()),
		mouse_x     = f32(rl.GetMouseX()),
		mouse_y     = f32(rl.GetMouseY()),
		scroll_axis = 0,
		mouse_left  = rl.IsMouseButtonDown(.LEFT),
		mouse_right = rl.IsMouseButtonDown(.RIGHT),
		delta       = f32(rl.GetFrameTime()),
		keys        = ui.Keys{
			up        = rl.IsKeyPressed(.UP),
			down      = rl.IsKeyPressed(.DOWN),
			left      = rl.IsKeyPressed(.LEFT),
			right     = rl.IsKeyPressed(.RIGHT),
			tab       = rl.IsKeyPressed(.TAB),
			esc       = rl.IsKeyPressed(.ESCAPE),
			ctrl      = rl.IsKeyPressed(.LEFT_CONTROL),
			delete    = rl.IsKeyPressed(.DELETE),
			ctrl_c    = rl.IsKeyDown(.LEFT_CONTROL) && rl.IsKeyPressed(.C),
			ctrl_v    = rl.IsKeyDown(.LEFT_CONTROL) && rl.IsKeyPressed(.V),
			ctrl_x    = rl.IsKeyDown(.LEFT_CONTROL) && rl.IsKeyPressed(.X),
			backspace = rl.IsKeyPressed(.BACKSPACE),
			enter     = rl.IsKeyPressed(.ENTER),
		},
	}

	qnt := 0
	for key := rl.GetCharPressed(); key > 0; key = rl.GetCharPressed() {
		ui_frame.text_input_buffer[qnt] = key
		qnt += 1
	}
	ui_frame.text_input_buffer_used = qnt

	ui.init_frame(ui_frame)
}

Raylib_Render_Temporaries :: struct {
	scroll_offset: [2]f32,
}

temp: Raylib_Render_Temporaries

draw_text :: proc(pos: [2]f32, text: string) {
	rl.DrawTextEx(default_font, strings.clone_to_cstring(text, context.temp_allocator),
		{pos.x, pos.y}, DEFAULT_FONT_SIZE, 2, rl.WHITE)
}

get_mouse_pos :: proc() -> (pos: [2]f32) {
	pos.x = f32(rl.GetMouseX())
	pos.y = f32(rl.GetMouseY())
	return
}

mouse_left_down :: proc() -> bool {
	return rl.IsMouseButtonPressed(.LEFT)
}

mouse_right_down :: proc() -> bool {
	return rl.IsMouseButtonPressed(.RIGHT)
}

ctrl_pressed :: proc() -> bool {
	return rl.IsKeyDown(.LEFT_CONTROL)
}

@(private)
textures: [256]rl.Texture

draw_image_buffer :: proc(pos: [2]f32, data: rawptr, w, h: int, scale: f32 = 1, id := 0) {
	im := rl.Image {
		data = data,
		width = i32(w),
		height = i32(h),
		mipmaps = 1,
		format = .UNCOMPRESSED_R8G8B8A8,
	}

	if textures[id].id == 0 {
		textures[id] = rl.LoadTextureFromImage(im)
	} else {
		rl.UpdateTexture(textures[id], data)
	}

	tex := textures[id]
	rl.DrawTexturePro(
		tex,
		{0, 0, f32(tex.width), -f32(tex.height)},
		{pos.x, pos.y, f32(tex.width) * scale, f32(tex.height) * scale},
		{0, 0},
		0,
		rl.WHITE,
	)
}

raylib_render_default :: proc() {
	for p in ui.all_buffered_primitives() {
		#partial switch p.kind {
		case .Cursor:
			c := rl.ORANGE
			c.a = byte(p.cursor.opacity * 255)
			rl.DrawRectangle(i32(p.cursor.pos.x), i32(p.cursor.pos.y), 2, i32(p.cursor.size), c)
		case .Text:
			rl.DrawTextEx(default_font, strings.clone_to_cstring(p.text.content),
				{p.text.pos.x, p.text.pos.y}, DEFAULT_FONT_SIZE, 2, rl.WHITE)
		case .Rect_Button:
			c: rl.Color
			switch p.rect.mouse_state {
				case .Idle:         c = rl.GRAY
				case .Clicked:      c = rl.DARKBLUE
				case .Hovering:     c = rl.GRAY
				case .Holding:      c = rl.BLUE
				case .Fake_Holding: c = rl.GRAY
			}

			q := p.rect.glow / 2.5 + 1
			c = rl.Color{byte(f32(c.r) * q), byte(f32(c.g) * q), byte(f32(c.b) * q), 255}
			darker_c := rl.Color{c.r / 2, c.g / 2, c.b / 2, 255}

			rl.DrawRectangle(i32(p.rect.bounds.x), i32(p.rect.bounds.y), i32(p.rect.bounds.w), i32(p.rect.bounds.h), c)
			rl.DrawRectangleLines(i32(p.rect.bounds.x), i32(p.rect.bounds.y), i32(p.rect.bounds.w), i32(p.rect.bounds.h), darker_c)
		case .Rect_Input:
			c: rl.Color
			switch p.rect.mouse_state {
				case .Idle:         c = rl.GRAY
				case .Clicked:      c = rl.DARKBLUE
				case .Hovering:     c = rl.GRAY
				case .Holding:      c = rl.BLUE
				case .Fake_Holding: c = rl.GRAY
			}

			q := p.rect.glow / 2.5 + 1
			c = rl.Color{byte(f32(c.r) * q), byte(f32(c.g) * q), byte(f32(c.b) * q), 255}
			darker_c := rl.Color{c.r / 2, c.g / 2, c.b / 2, 255}

			rl.DrawRectangle(i32(p.rect.bounds.x), i32(p.rect.bounds.y), i32(p.rect.bounds.w), i32(p.rect.bounds.h), c)
			rl.DrawRectangleLines(i32(p.rect.bounds.x), i32(p.rect.bounds.y), i32(p.rect.bounds.w), i32(p.rect.bounds.h), darker_c)
			
			if p.rect.focus_glow > 0.01 {
				q = p.rect.focus_glow
				c = rl.ORANGE
				c = rl.Color{byte(f32(c.r) * q), byte(f32(c.g) * q), byte(f32(c.b) * q), 255}
				rl.DrawRectangleLines(i32(p.rect.bounds.x), i32(p.rect.bounds.y), i32(p.rect.bounds.w), i32(p.rect.bounds.h), c)
			}
		case .Rect_Tabbed_Left:
			q := p.rect.glow / 2.5
			c := rl.Color{byte(f32(255) * q), byte(f32(255) * q), byte(f32(255) * q), 255}
			c2 := rl.Color{c.r + 64, c.g + 64, c.b + 64, 255}

			rl.DrawRectangleGradientH(i32(p.rect.bounds.x), i32(p.rect.bounds.y), i32(p.rect.bounds.w), i32(p.rect.bounds.h), c, {c.r, c.g, c.b, 32})
			rl.DrawRectangle(i32(p.rect.bounds.x), i32(p.rect.bounds.y), i32(5.0 + p.rect.glow * 5), i32(p.rect.bounds.h), c2)
		case .Rect_Marker_Checked:
			c := rl.ORANGE

			rl.DrawRectangle(i32(p.rect.bounds.x), i32(p.rect.bounds.y), i32(p.rect.bounds.w), i32(p.rect.bounds.h), c)
		case .Rect_Marker_Unchecked:
			c := rl.GRAY

			q := p.rect.glow / 2.5 + 1
			c = rl.Color{byte(f32(c.r) * q), byte(f32(c.g) * q), byte(f32(c.b) * q), 255}

			rl.DrawRectangle(i32(p.rect.bounds.x), i32(p.rect.bounds.y), i32(p.rect.bounds.w), i32(p.rect.bounds.h), c)
		}
	}
}

