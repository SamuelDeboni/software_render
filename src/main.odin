package main

import "core:log"
import "core:sync"
import "core:thread"
import "core:fmt"
import "core:math/linalg"
import sr "srender"
import rl "vendor:raylib"

import "ui_toolkit/ui"
import uitk "ui_toolkit"

CANVAS_SIZE :: (720 / 2)

potato_data := #load("../assets/potato.png")

main :: proc() {
	context.logger = log.create_console_logger()

	ui_ctx: ui.Context
	uitk.create_window(1080, 720, "Trabalho CG", &ui_ctx)
	// Set Window Icon
	{
		im := rl.LoadImageFromMemory(".PNG", &potato_data[0], cast(i32)len(potato_data))
		rl.SetWindowIcon(im)
	}

	sr.init({CANVAS_SIZE, CANVAS_SIZE})

	monke_data := #load("monke.obj")
	monke := sr.mesh_from_obj_data(monke_data)
	timer: f32

	for !uitk.window_should_close()	{
		timer += 0.016

		uitk.init_frame()

		sr.clear(sr.BLACK)

		// Draw axis
		for i in i32(1)..<CANVAS_SIZE {
			sr.set_pixel({CANVAS_SIZE/2, i}, sr.BLUE)
			if i % 10 == 0 {
				sr.set_pixel({CANVAS_SIZE/2 + 1, i}, sr.BLUE)
				sr.set_pixel({CANVAS_SIZE/2 - 1, i}, sr.BLUE)
			}
		}

		for i in i32(1)..<CANVAS_SIZE {
			sr.set_pixel({i, CANVAS_SIZE/2}, sr.RED)
			if i % 10 == 0 {
				sr.set_pixel({i, CANVAS_SIZE/2 + 1}, sr.RED)
				sr.set_pixel({i, CANVAS_SIZE/2 - 1}, sr.RED)
			}
		}


		ui.split(0, CANVAS_SIZE * 2 + 20, ui.FILL_W, 20)
		ui.next()
		ui.space(20)

		if label_checkbox("MONKE!").checked {
			draw_demo_mesh(monke, timer)
		}

		ui.space(10)

		points_transform()

		uitk.draw_image_buffer({0, 0}, &sr.surface.buffer[0], cast(int)sr.surface.size.x, cast(int)sr.surface.size.y, 2)

		rl.DrawFPS(0, 0)
		uitk.end_frame()
	}
}

label_checkbox :: proc(label: string, loc := #caller_location) -> ui.WData {
	ui.split(0, 40, ui.FILL_W)
	defer ui.pop()

	data := ui.checkbox(loc = loc)
	ui.next()
	ui.space(6)
	ui.label(label)
	return data
}

point_edit :: proc (vec: ^[2]f32, inc: f32, loc := #caller_location) {
	ui.split(ui.LEFT, ui.P(0.5), ui.P(0.5)); defer ui.pop()
	{
		ui.split(ui.LEFT, 20, 32, 8, 48, 32); defer ui.pop()

		ui.space(6)
		ui.label("X:")
		ui.next()
		if ui.button("<", ui.IDXID(0), loc).down_clicked do vec.x -= inc
		ui.next()
		ui.next()
		ui.space(6)
		ui.label(fmt.tprintf("%.1f", vec.x))
		ui.next()
		if ui.button(">", ui.IDXID(1), loc).down_clicked do vec.x += inc
	}
	ui.next()

	{
		ui.split(ui.LEFT, 20, 32, 8, 48, 32); defer ui.pop()

		ui.space(6)
		ui.label("Y:")
		ui.next()
		if ui.button("<", ui.IDXID(2), loc).down_clicked do vec.y -= inc
		ui.next()
		ui.next()
		ui.space(6)
		ui.label(fmt.tprintf("%.1f", vec.y))
		ui.next()
		if ui.button(">", ui.IDXID(3), loc).down_clicked do vec.y += inc
	}
}


line_dda :: proc(p0: [2]f32, p1: [2]f32, color := sr.WHITE) {
	p0 := p0
	p1 := p1

	swap_if_greater :: proc(a: ^[2]f32, b: ^[2]f32, c: int) {
		if a[c] > b[c] {
			tmp := a^
			a^ = b^
			b^ = tmp
		}
	}

	if abs(p1.x - p0.x) > abs(p1.y - p0.y) {
		swap_if_greater(&p0, &p1, 0)

		delta := (p1.y - p0.y) / (p1.x - p0.x)
		y := p0.y

		for x := p0.x; x <= p1.x; x += 1 {
			sr.set_pixelf({x, y}, color)
			y += delta
		}
	} else {
			swap_if_greater(&p0, &p1, 1)

			delta := (p1.x - p0.x) / (p1.y - p0.y)
			x := p0.x

			for y := p0.y; y <= p1.y; y += 1 {
				sr.set_pixelf({x, y}, color)
				x += delta
			}
	}
}

bresenham_circle :: proc(pos: [2]f32, radius: f32, color := sr.WHITE) {
	posi := [2]i32{i32(pos.x), i32(pos.y)}
	r := i32(radius)

	draw_points :: proc(pos_x, pos_y, x, y: i32, color := sr.WHITE) {
		sr.set_pixel({pos_x + x, pos_y + y}, color)
		sr.set_pixel({pos_x - x, pos_y + y}, color)
		sr.set_pixel({pos_x + x, pos_y - y}, color)
		sr.set_pixel({pos_x - x, pos_y - y}, color)
		sr.set_pixel({pos_x + y, pos_y + x}, color)
		sr.set_pixel({pos_x - y, pos_y + x}, color)
		sr.set_pixel({pos_x + y, pos_y - x}, color)
		sr.set_pixel({pos_x - y, pos_y - x}, color)
	}

	x: i32
	y: i32 = r
	d := 3 - 2 * r

	draw_points(posi.x, posi.y, x, y, color)
	for y >= x {
		x += 1
		if d > 0 {
			d += 4 * (x - y) + 10
			y -= 1
		} else {
			d += 4 * x + 6
		}
		draw_points(posi.x, posi.y, x, y, color)
	}
}

bresenham_line :: proc(p0: [2]f32, p1: [2]f32, color := sr.WHITE) {
	dx := i32(p1.x - p0.x)
	dy := i32(p1.y - p0.y)

	incrx: i32
	incry: i32

	if (dx >= 0) {
		incrx = 1
	} else {
		incrx = -1
		dx = -dx
	}

	if (dy >= 0) {
		incry = 1
	} else{
		incry = -1
		dy = -dy
	}

	x := i32(p0.x)
	y := i32(p0.y)
	sr.set_pixel({x, y}, color)

	if dy < dx {
		p := 2 * dy - dx
		const1 := 2 * dy
		const2 := 2 * (dy - dx)

		for i := 0; i < int(dx); i += 1 {
			x += incrx
			if p < 0 {
				p += const1
			} else {
				y += incry
				p+= const2
			}
			sr.set_pixel({x, y}, color)
		}

	} else {
		p := 2 * dx - dy
		const1 := 2 * dx
		const2 := 2 * (dx - dy)

		for i := 0; i < int(dy); i += 1 {
			y += incry
			if p < 0 {
				p += const1
			} else {
				x += incrx
				p+= const2
			}
			sr.set_pixel({x, y}, color)
		}
	}
}

points_transform :: proc() {
	MAX_POINTS :: 64

	@static points: Static_List([3]f32, MAX_POINTS)
	@static indices: Static_List(i32, MAX_POINTS * 2)
	@static circle_indices: Static_List(i32, MAX_POINTS * 2)
	@static selecting_point := false
	@static t_position: [2]f32
	@static t_rotation: f32
	@static t_scale:    [2]f32 = {1, 1}

	mouse_wheel := rl.GetMouseWheelMove()

		// Calculate mouse pos in the canvas
	canvas_cursor: [2]f32
	{
		canvas_cursor = uitk.get_mouse_pos() * 0.5
		canvas_cursor.y = CANVAS_SIZE - canvas_cursor.y
	}

	// Calculate transform matrix
	transform_m: matrix[3, 3]f32
	pos_m := matrix[3, 3]f32 {
		1, 0, t_position.x,
		0, 1, t_position.y,
		0, 0, 1,
	}

	pos2_m := matrix[3, 3]f32 {
		1, 0, CANVAS_SIZE/2,
		0, 1, CANVAS_SIZE/2,
		0, 0, 1,
	}

	scl_m := matrix[3, 3]f32 {
		t_scale.x, 0, 0,
		0, t_scale.y, 0,
		0, 0, 1,
	}

	theta := t_rotation / 360
	rot_m := matrix[3, 3]f32 {
		tcos(theta), -tsin(theta), 0,
		tsin(theta),  tcos(theta), 0,
		0, 0, 1,
	}
	transform_m = pos2_m * rot_m * scl_m * pos_m

	@static prev_canvas_cursor: [2]f32
	defer prev_canvas_cursor = canvas_cursor

	if rl.IsKeyDown(.LEFT_CONTROL) {
		t_rotation += mouse_wheel * 5
	} else {
		if mouse_wheel != 0 {
			t_scale += t_scale * mouse_wheel * 0.1
		}
	}

	// Calculate canvas delta
	canvas_cursor_delta := canvas_cursor - prev_canvas_cursor

	// Place point button
	if !selecting_point {
		if ui.button("Place Points").clicked {
			selecting_point = true
		}
	} else {
		ui.button("Click to place point - Esc to stop")
	}

	ui.space(20)
	transform_enabled := !label_checkbox("Disable Transform").checked

	// Place point logic
	if selecting_point {

		if canvas_cursor.x >= 0 && canvas_cursor.y >= 0 &&
			 canvas_cursor.x < CANVAS_SIZE && canvas_cursor.y < CANVAS_SIZE
		{
			sr.fill_circle({cast(i32)canvas_cursor.x, cast(i32)canvas_cursor.y}, 5, sr.GREEN)

			point := [3]f32{0, 0, 1}
			point.xy = canvas_cursor

			if transform_enabled {
				point = inverse(transform_m) * point
			}

			if rl.IsKeyDown(.LEFT_SHIFT) && points.len > 0 {
				bresenham_line((transform_m * points.data[points.len - 1]).xy, canvas_cursor.xy, sr.YELOW)
			}

			if rl.IsKeyDown(.C) && points.len > 0 {
				p := (transform_m * points.data[points.len - 1]).xy
				r := linalg.length(p - canvas_cursor.xy)
				bresenham_circle(p, r, sr.WHITE)
			}

			if uitk.mouse_left_down() {
				if rl.IsKeyDown(.LEFT_SHIFT) && points.len < len(points.data){
					sl_push(&indices, i32(points.len - 1))
					sl_push(&indices, i32(points.len))
				}

				if rl.IsKeyDown(.C) && points.len < len(points.data){
					sl_push(&circle_indices, i32(points.len - 1))
					sl_push(&circle_indices, i32(points.len))
				}

				sl_push(&points, point)
			}

			if rl.IsKeyPressed(.ESCAPE) {
				selecting_point = false
			}
		} else if uitk.mouse_left_down() {
			selecting_point = false
		}
	}

	points_transformed := points
	points_slice := sl_slice(&points_transformed)

	inc_multi: f32 = 10.0 if rl.IsKeyDown(.LEFT_CONTROL) else 1.0

	ui.space(20)
	ui.label("Position:")
	point_edit(&t_position, 1 * inc_multi)

	ui.space(20)
	ui.label("Scale:")
	point_edit(&t_scale, 0.1 * inc_multi)

	ui.space(20)
	{
		ui.split(ui.LEFT, 96, 32, 8, 48, 32); defer ui.pop()
		ui.space(6)
		ui.label("Rotation:")
		ui.next()
		if ui.button("<").down_clicked do t_rotation -= 1 * inc_multi
		ui.next()
		ui.next()
		ui.space(6)
		ui.label(fmt.tprintf("%.0f", t_rotation))
		ui.next()
		if ui.button(">").down_clicked do t_rotation += 1  * inc_multi
		ui.next()
	}

	// Use mouse to manipulate the canvas
	if rl.IsMouseButtonDown(.MIDDLE) && transform_enabled {
		v: [3]f32 = {0, 0, 1}
		v.xy = canvas_cursor_delta
		t_position += (inverse(rot_m) * inverse(scl_m) * v).xy
	}

	// Transform points
	if transform_enabled {
		for _, idx in points_slice {
			point := &points_slice[idx]
			point^ = transform_m * point^
		}
	} else {
		for _, idx in points_slice {
			point := &points_slice[idx]
			point^ = pos2_m * point^
		}
	}

	colors := []sr.Color {
		{0, 0,   255,   255},
		{0, 255, 0,     255},
		{0, 255, 255,   255},
		{255, 0,   0,   255},
		{255, 0,   255, 255},
		{255, 255, 0,   255},

		{  0, 255, 128, 255},
		{  0, 128, 255, 255},
		{128,   0, 255, 255},
		{255,   0, 128, 255},
		{128, 255,   0, 255},
		{255, 128,   0, 255},
	}


	// Draw Circles
	for i := 0; i < circle_indices.len; i += 2 {
		i0 := circle_indices.data[i]
		i1 := circle_indices.data[i+1]

		p := points_slice[i0].xy
		r := linalg.length(points_slice[i0].xy - points_slice[i1].xy)
		bresenham_circle(p, r, colors[(i/2)%len(colors)])
	}

	// Draw Lines
	for i := 0; i < indices.len; i += 2 {
		i0 := indices.data[i]
		i1 := indices.data[i+1]

		bresenham_line(points_slice[i0].xy, points_slice[i1].xy, colors[(i/2)%len(colors)])
	}


	// Draw points
	for p, idx in points_slice {
		sr.set_pixelf(p.xy, sr.WHITE)

		sr.set_pixelf(p.xy + {1, 0}, sr.WHITE)
		sr.set_pixelf(p.xy - {1, 0}, sr.WHITE)
		sr.set_pixelf(p.xy + {0, 1}, sr.WHITE)
		sr.set_pixelf(p.xy - {0, 1}, sr.WHITE)
	}



	// Instruções
	ui.space(30)
	ui.label("== Instrucoes ==")
	ui.space(10)
	ui.label("Botoes da UI:")
	ui.label("  CTRL + click para aumentar o incremento")
	ui.space(10)
	ui.label("Ao colocar pontos:")
	ui.label("  Mouse Esquerdo coloca ponto")
	ui.label("  Shift para colocar linha")
	ui.label("  C para colocar circunferencia")
	ui.label("  ESC sai do modo de colocar pontos")
	ui.space(10)
	ui.label("Transformacoes:")
	ui.label("  Botao do meio e arrastar -> translacao")
	ui.label("  Scroll -> Escala")
	ui.label("  Scroll + Ctrl -> Rotacao")

}

draw_demo_mesh :: proc(mesh: sr.Mesh, timer: f32) {
	for v in mesh.v {
		sr.push_vert(v)
	}

	for i in mesh.i {
		sr.push_index(i)
	}

	y_rot := timer * 0.1
	rot_y := matrix[4, 4]f32 {
		tcos(y_rot), 0, -tsin(y_rot), 0,
		0, 1, 0, 0,
		tsin(y_rot), 0, tcos(y_rot), 0,
		0, 0, 0, 1,
	}

	x_rot :f32= -0.25
	rot_x := matrix[4, 4]f32 {
		1, 0, 0, 0,
		0, tcos(x_rot), -tsin(x_rot), 0,
		0, tsin(x_rot), tcos(x_rot), 0,
		0, 0, 0, 1,
	}

	m0 := matrix[4, 4]f32 {
		1,  0,  0,  0.5,
		0,  1,  0,  0.5,
		0,  0,  1, -5,
		0,  0,  0,  1,
	}
	m1 := matrix[4, 4]f32 {
		1,  0,  0,   0,
		0,  1,  0,   0,
		0,  0,  1,   0,
		0,  0,   0,   1,
	}

	n : f32 = 1
	f : f32 = 5
	mp := matrix[4, 4]f32 {
		n * 360, 0, 0, 0,
		0, n * 360, 0, 0,
		0, 0, 1/n, 1,
		0, 0,-1, 0,
	}

	sr.transform_buffer(m0 * m1 * rot_y * rot_x)
	sr.transform_normals(m1 * rot_y * rot_x)
	sr.set_ambient_light({0.05, 0.05, 0.05})
	sr.add_directional_light({1, tcos(timer), 0}, {1, 0, 0})
	sr.add_directional_light({0, -1, 0}, {1, 1, 0})
	sr.add_directional_light({-1, 0, 0}, {0, 0, 1})
	sr.project_buffer(mp)
	sr.flush_buffer()
}
