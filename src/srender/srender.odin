package srender

import "core:log"
import "core:simd"
import "core:math"
import "core:math/linalg"

Color :: [4]u8

RED :: Color{255, 0, 0, 255}
GREEN :: Color{0, 255, 0, 255}
BLUE :: Color{0, 0, 255, 255}
WHITE :: Color{255, 255, 255, 255}
ORANGE :: Color{255, 128, 0, 255}
YELOW :: Color{255, 255, 0, 255}
BLACK :: Color{0, 0, 0, 255}
GRAY :: Color{128, 128, 128, 255}
DARKBLUE :: Color{0, 0, 180, 255}

Render_Surface :: struct {
	size:   [2]i32,
	buffer: []Color,
	zbuffer: []f32,
}

Image :: struct {
	w, h:   i32,
	pixels: []Color,
}

MAX_VERTICES :: (65535)
MAX_INDICES :: (65535)

Vertex :: struct {
	pos:   [4]f32,
	normal: [4]f32,
	color: [4]f32,
}

color_float :: proc(color: Color) -> [4]f32 {
	M :: 1.0 / 255.0
	return [4]f32{f32(color.r) * M, f32(color.g) * M, f32(color.b) * M, f32(color.a) * M}
}

vertexc :: proc(pos: [3]f32, color: Color = WHITE) -> Vertex {
	posn: [4]f32
	posn.xyz = pos
	posn.w = 1
	return Vertex{pos = posn, color = color_float(color)}
}

Render_Buffer :: struct {
	vertex_buffer: #soa[MAX_VERTICES]Vertex,
	index_buffer:  [MAX_INDICES]u16,
	vertex_count:  int,
	index_count:   int,
}

surface: Render_Surface
render_buffer: ^Render_Buffer

init :: proc(canvas_size: [2]i32, al := context.allocator) {
	using surface

	size = canvas_size
	buffer = make([]Color, size.x * size.y, al)
	zbuffer = make([]f32, size.x * size.y, al)

	render_buffer = new(Render_Buffer, al)
}

clear :: proc(color: Color) {
	using surface

	#no_bounds_check for _, idx in buffer[:] {
		buffer[idx] = color
	}

	#no_bounds_check for _, idx in zbuffer[:] {
		zbuffer[idx] = 0
	}
}

fill_circle :: proc(pos: [2]i32, diameter: i32, color: Color) {
	radius := diameter / 2
	x0 := clamp(pos.x - radius, 0, surface.size.x - 1)
	y0 := clamp(pos.y - radius, 0, surface.size.y - 1)
	x1 := clamp(pos.x + radius, 0, surface.size.x - 1)
	y1 := clamp(pos.y + radius, 0, surface.size.y - 1)

	offset := y0 * surface.size.x
	for y := y0; y <= y1; y += 1 {
		yc := y - pos.y
		for x := x0; x <= x1; x += 1 {
			xc := x - pos.x
			if (xc * xc + yc * yc) * 4 < (diameter * diameter) {
				surface.buffer[x + offset] = color
			}
		}
		offset += surface.size.x
	}
}

texture_map :: #force_inline proc(uv: [2]f32, tex: Image) -> (col: Color) {
	uvi: [2]i32

	uvi.x = clamp(i32(uv.x * f32(tex.w)), 0, tex.w - 1)
	uvi.y = tex.h - clamp(i32((uv.y) * f32(tex.h)), 0, tex.h - 1) - 1

	return tex.pixels[uvi.x + uvi.y * tex.w]
}

set_pixel :: #force_inline proc(pos: [2]i32, color: Color) {
	if pos.x < 0 || pos.y < 0 || pos.x >= surface.size.x || pos.y >= surface.size.y do return
	surface.buffer[pos.x + pos.y * surface.size.x] = color
}

set_pixelf :: #force_inline proc(pos: [2]f32, color: Color) {
	set_pixel({i32(pos.x), i32(pos.y)}, color)
}

flush_buffer :: proc() {
	using render_buffer
	draw_buffer()
	vertex_count = 0
	index_count = 0
}

push_vert :: proc(vert: Vertex) {
	using render_buffer

	if vertex_count < MAX_VERTICES {
		vertex_buffer[vertex_count] = vert
		vertex_count += 1
	}
}

push_index :: proc(index: u16) {
	using render_buffer

	if index_count < MAX_INDICES {
		index_buffer[index_count] = index
		index_count += 1
	}
}

push_indices :: proc(indices: ..u16) {
	using render_buffer

	for i in indices {
		push_index(i)
	}
}

transform_buffer :: proc(mat: matrix[4, 4]f32) {
	using render_buffer
	if vertex_count == 0 do return

	for _, idx in vertex_buffer[:vertex_count] {
		vertex_buffer[idx].pos = mat * vertex_buffer[idx].pos
	}
}

transform_normals :: proc(mat: matrix[4, 4]f32) {
	using render_buffer
	if vertex_count == 0 do return

	mat := (mat)

	for _, idx in vertex_buffer[:vertex_count] {
		vertex_buffer[idx].normal = linalg.normalize(mat * vertex_buffer[idx].normal)
		/*
		vertex_buffer[idx].color.r = cast(u8)(vertex_buffer[idx].normal.x * 100 + 100)
		vertex_buffer[idx].color.g = cast(u8)(vertex_buffer[idx].normal.y * 100 + 100)
		vertex_buffer[idx].color.b = cast(u8)(vertex_buffer[idx].normal.z * 100 + 100)
		vertex_buffer[idx].color.a = 255
		*/
	}
}

set_ambient_light :: proc(light: [3]f32) {
	using render_buffer

	for _, idx in vertex_buffer[:vertex_count] {
		vertex_buffer[idx].color.rgb *= light
	}
}

add_directional_light :: proc(light_dir: [3]f32, light_color := [3]f32{1, 1, 1}) {
	using linalg
	using render_buffer

	light_dir := light_dir
	light_dir = normalize(light_dir)

	for _, idx in vertex_buffer[:vertex_count] {
		intensity := max(dot(light_dir, vertex_buffer[idx].normal.xyz), 0)
		vertex_buffer[idx].color.rgb += light_color * intensity
	}
}

project_buffer :: proc(mat: matrix[4, 4]f32) {
	using render_buffer
	if vertex_count == 0 do return

	for _, idx in vertex_buffer[:vertex_count] {
		vertex_buffer[idx].pos = mat * vertex_buffer[idx].pos

		vertex_buffer[idx].pos.x -= cast(f32)surface.size.x * 0.5
		vertex_buffer[idx].pos.y -= cast(f32)surface.size.y * 0.5
		vertex_buffer[idx].pos = vertex_buffer[idx].pos / -vertex_buffer[idx].pos.z
		vertex_buffer[idx].pos.x += cast(f32)surface.size.x * 0.5
		vertex_buffer[idx].pos.y += cast(f32)surface.size.y * 0.5

	}
}

draw_buffer :: proc() {
	using render_buffer
	if index_count < 3 do return

	for i := 0; i < index_count; i += 3 {
		indices := index_buffer[i:][:3]

		idx_0 := indices[0]
		idx_1 := indices[1]
		idx_2 := indices[2]

		if idx_0 >= MAX_VERTICES ||
			 idx_1 >= MAX_VERTICES ||
			 idx_2 >= MAX_VERTICES
		{
			continue
		}

		p0 := vertex_buffer[idx_0].pos.xy
		p1 := vertex_buffer[idx_1].pos.xy
		p2 := vertex_buffer[idx_2].pos.xy

		z0 := vertex_buffer[idx_0].pos.w
		z1 := vertex_buffer[idx_1].pos.w
		z2 := vertex_buffer[idx_2].pos.w
		//log.debug(z0)

		z0_4x := #simd[4]f32{z0, z0, z0, z0}
		z1_4x := #simd[4]f32{z1, z1, z1, z1}
		z2_4x := #simd[4]f32{z2, z2, z2, z2}

		p0.x = math.floor(p0.x)
		p0.y = math.floor(p0.y)

		p1.x = math.floor(p1.x)
		p1.y = math.floor(p1.y)

		p2.x = math.floor(p2.x)
		p2.y = math.floor(p2.y)

		clamp_color :: proc(c: [4]f32) -> [4]f32 {
			return {
				clamp(c.r, 0, 1),
				clamp(c.g, 0, 1),
				clamp(c.b, 0, 1),
				clamp(c.a, 0, 1),
			}
		}

		c0 := clamp_color(vertex_buffer[indices[0]].color)
		c1 := clamp_color(vertex_buffer[indices[1]].color)
		c2 := clamp_color(vertex_buffer[indices[2]].color)

		c0_r_4x := #simd[4]f32{c0.r, c0.r, c0.r, c0.r}
		c1_r_4x := #simd[4]f32{c1.r, c1.r, c1.r, c1.r}
		c2_r_4x := #simd[4]f32{c2.r, c2.r, c2.r, c2.r}

		c0_g_4x := #simd[4]f32{c0.g, c0.g, c0.g, c0.g}
		c1_g_4x := #simd[4]f32{c1.g, c1.g, c1.g, c1.g}
		c2_g_4x := #simd[4]f32{c2.g, c2.g, c2.g, c2.g}

		c0_b_4x := #simd[4]f32{c0.b, c0.b, c0.b, c0.b}
		c1_b_4x := #simd[4]f32{c1.b, c1.b, c1.b, c1.b}
		c2_b_4x := #simd[4]f32{c2.b, c2.b, c2.b, c2.b}

		c0_a_4x := #simd[4]f32{c0.a, c0.a, c0.a, c0.a}
		c1_a_4x := #simd[4]f32{c1.a, c1.a, c1.a, c1.a}
		c2_a_4x := #simd[4]f32{c2.a, c2.a, c2.a, c2.a}

		using surface

		x0 := min(p0.x, p1.x, p2.x)
		y0 := min(p0.y, p1.y, p2.y)
		x1 := max(p0.x, p1.x, p2.x)
		y1 := max(p0.y, p1.y, p2.y)

		x0 = clamp(x0, 0, cast(f32)size.x - 3)
		y0 = clamp(y0, 0, cast(f32)size.y)
		x1 = clamp(x1, 0, cast(f32)size.x - 3)
		y1 = clamp(y1, 0, cast(f32)size.y)

		offset := cast(i32)y0 * size.x + cast(i32)x0

		orient2d :: #force_inline proc(v0, v1, v2: [2]f32) -> f32 {
			a := v1 - v0
			b := v2 - v0
			return a.x * b.y - a.y * b.x
		}

		triangle_orient := orient2d(p0, p1, p2)

		// If negative the triangle is clockwise
		// If 0 two or more points are on the same line
		if triangle_orient <= 0 do continue

		one_over_orient := 1.0 / triangle_orient
		one_over_orient_4x := #simd[4]f32{
			one_over_orient,
			one_over_orient,
			one_over_orient,
			one_over_orient,
		}

		w0_mul_x := p1.y - p2.y
		w1_mul_x := p2.y - p0.y
		w2_mul_x := p0.y - p1.y

		w0_mul_x_4x := #simd[4]f32{w0_mul_x, w0_mul_x, w0_mul_x, w0_mul_x}
		w1_mul_x_4x := #simd[4]f32{w1_mul_x, w1_mul_x, w1_mul_x, w1_mul_x}
		w2_mul_x_4x := #simd[4]f32{w2_mul_x, w2_mul_x, w2_mul_x, w2_mul_x}

		S0123 :: #simd[4]f32{0, 1, 2, 3}
		S3210 :: #simd[4]f32{3, 2, 1, 0}
		S0 :: #simd[4]f32{0, 0, 0, 0}
		S4 :: #simd[4]f32{4, 4, 4, 4}
		S255 :: #simd[4]f32{255, 255, 255, 255}

		w0_mul_y := p2.x - p1.x
		w1_mul_y := p0.x - p2.x
		w2_mul_y := p1.x - p0.x

		w0_offset := p1.x * p2.y - p1.y * p2.x + x0 * w0_mul_x
		w1_offset := p2.x * p0.y - p2.y * p0.x + x0 * w1_mul_x
		w2_offset := p0.x * p1.y - p0.y * p1.x + x0 * w2_mul_x

		w0y := w0_offset + y0 * w0_mul_y
		w1y := w1_offset + y0 * w1_mul_y
		w2y := w2_offset + y0 * w2_mul_y

		size_x := i32(x1 - x0)
		//size_x += (size_x % 4)

		size_y := i32(y1 - y0)
		#no_bounds_check for y_i: i32 = 0; y_i < size_y; y_i += 1 {
			w0_4x := w0_mul_x_4x * S0123 + #simd[4]f32{w0y, w0y, w0y, w0y}
			w1_4x := w1_mul_x_4x * S0123 + #simd[4]f32{w1y, w1y, w1y, w1y}
			w2_4x := w2_mul_x_4x * S0123 + #simd[4]f32{w2y, w2y, w2y, w2y}

			y_index_offset := (y_i + i32(y0)) * size.x + i32(x0)

			for x_i: i32 = 0; x_i < size_x; x_i += 4 {
				l0 := w0_4x * one_over_orient_4x
				l1 := w1_4x * one_over_orient_4x
				l2 := w2_4x * one_over_orient_4x

				color_r := (c0_r_4x * l0 + c1_r_4x * l1 + c2_r_4x * l2) * S255
				color_g := (c0_g_4x * l0 + c1_g_4x * l1 + c2_g_4x * l2) * S255
				color_b := (c0_b_4x * l0 + c1_b_4x * l1 + c2_b_4x * l2) * S255
				color_a := (c0_a_4x * l0 + c1_a_4x * l1 + c2_a_4x * l2) * S255

				z_4x := z0_4x * l0 + z1_4x * l1 + z2_4x * l2

				ge0 := simd.lanes_ge(w0_4x, S0)
				ge1 := simd.lanes_ge(w1_4x, S0)
				ge2 := simd.lanes_ge(w2_4x, S0)
				ge := simd.and(ge0, simd.and(ge1, ge2))

				if simd.reduce_or(ge) != 0 {
					for i in i32(0) ..= 3 {
						z := simd.extract(z_4x, i)

						idx := x_i + y_index_offset + i
						if simd.extract(ge, i) != 0 && z >= zbuffer[idx] {
							r := simd.extract(color_r, i)
							g := simd.extract(color_g, i)
							b := simd.extract(color_b, i)
							a := simd.extract(color_a, i)

							buffer[idx] = {u8(r), u8(g), u8(b), u8(a)}
							//buffer[idx] = {u8(z * 100), u8(z * 100), u8(z * 100), 255}
							zbuffer[idx] = z
						}
					}
				}

				w0_4x += w0_mul_x_4x * S4
				w1_4x += w1_mul_x_4x * S4
				w2_4x += w2_mul_x_4x * S4
			}

			w0y += w0_mul_y
			w1y += w1_mul_y
			w2y += w2_mul_y
		}

		//set_pixel([2]i32{i32(p0.x), i32(p0.y)}, vertex_buffer[indices[0]].color)
		//set_pixel([2]i32{i32(p1.x), i32(p1.y)}, vertex_buffer[indices[1]].color)
		//set_pixel([2]i32{i32(p2.x), i32(p2.y)}, vertex_buffer[indices[2]].color)

		//draw_line(p0, p1, WHITE)
		//draw_line(p1, p2, WHITE)
		//draw_line(p2, p0, WHITE)
	}
}

Mesh :: struct {
	v: []Vertex,
	i: []u16,
}

import "core:strconv"
import "core:strings"

mesh_from_obj_data :: proc(
	obj: []byte,
	allocator := context.allocator,
	temp_allocator := context.allocator,
) -> (
	mesh: Mesh,
) {
	next_token :: proc(obj: ^[]byte, t: byte) -> (r: []byte, end: bool) {
		for i := 0; i < len(obj^); i += 1 {
			if obj[i] == t {
				r = obj[:i]
				obj^ = obj[i + 1:]
				return
			}
		}

		r = obj[:]
		end = true
		return
	}

	v_count: int
	i_count: int
	uv_count: int
	n_count: int

	data := obj
	for {
		line, end := next_token(&data, '\n')
		if end do break

		if line[0] == '#' || line[0] == 'o' do continue

		if line[0] == 'v' {
			if line[1] == 't' {
				uv_count += 1
			} else if line[1] == 'n' {
				n_count += 1
			} else {
				v_count += 1
			}
			continue
		}

		if line[0] == 'f' {
			i_count += 3
			continue
		}
	}

	v := make([][3]f32, v_count, temp_allocator)
	i := make([][3]u32, i_count, temp_allocator)
	n := make([][3]f32, n_count, temp_allocator)
	uv := make([][2]f32, uv_count, temp_allocator)

	v_i, i_i, n_i, uv_i: int

	data = obj
	for {
		line, end := next_token(&data, '\n')
		if end do break

		if line[0] == '#' || line[0] == 'o' do continue

		tok, e := next_token(&line, ' ')
		if e do continue

		if tok[0] == 'v' {
			if len(tok) == 1 {
				for idx in 0 ..= 2 {
					t, _ := next_token(&line, ' ')
					v[v_i][idx], _ = strconv.parse_f32(strings.string_from_ptr(&t[0], len(t)))
				}
				v_i += 1
			} else if tok[1] == 't' {
				for idx in 0 ..= 1 {
					t, _ := next_token(&line, ' ')
					uv[uv_i][idx], _ = strconv.parse_f32(strings.string_from_ptr(&t[0], len(t)))
				}
				uv_i += 1
			} else if tok[1] == 'n' {
				for idx in 0 ..= 2 {
					t, _ := next_token(&line, ' ')
					n[n_i][idx], _ = strconv.parse_f32(strings.string_from_ptr(&t[0], len(t)))
				}
				n_i += 1
			}

			continue
		}

		if tok[0] == 'f' {
			for _ in 0 ..= 2 {
				t, _ := next_token(&line, ' ')

				for j in 0 ..= 2 {
					t0, _ := next_token(&t, '/')
					if len(t0) == 0 do continue
					value, ok := strconv.parse_uint(strings.string_from_ptr(&t0[0], len(t0)))
					i[i_i][j] = u32(value) - 1
				}

				i_i += 1
			}

			continue
		}
	}

	v_idx: int = 0
	if len(uv) > v_count {
		v_idx = 1
		v_count = len(uv)
	}

	/*
    if len(n) > v_count {
        v_idx = 2
        v_count = len(n)
    }
    */

	mesh.v = make([]Vertex, i_count, allocator)
	mesh.i = make([]u16, i_count)

	for index, idx in i[:] {
		mesh.v[idx].pos.xyz = v[index[0]]
		mesh.v[idx].pos.w = 1

		//mesh.v[idx].uv = uv[index[1]]
		mesh.v[idx].color = color_float(WHITE)
		mesh.v[idx].normal.xyz = n[index[2]]
		mesh.v[idx].normal.w = 1
		mesh.i[idx] = u16(idx)
	}

	return
}
